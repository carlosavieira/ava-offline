# AVA-Offline

## Download

To download the project files, install [git](https://gitlab.com/nied/AVA-Offline/wikis/instalar-git) and execute:

```bash
git clone https://gitlab.com/nied/AVA-Offline.git
```

## WebApp Dev Setup

Requirements:

- NodeJS 12+
- @angular/cli

After cloning the project, download the project dependencies by running:

```bash
cd webapp/
npm ci
```

Finally, to run a local server, execute:

```bash
ng serve
```

You can access the webapp in [localhost:4200](http://localhost:4200/)

## API Dev Setup

## Fast-way: using Visual Studio Code and dev containers

If you are using Windows this is by far the fastest way to do it.

### Setup Docker

- [Install Docker Desktop](https://www.docker.com/products/docker-desktop)
- Ensure that docker is running, and in the docker desktop settings the folder for the project is shared. (Docker icon on system tray -> Settings -> Resources -> File Sharing)
- In Visual Studio Code, install the [Remote - Containers extension](https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.remote-containers)

### Running

- Open vscode on the **/api** folder and you should be asked to *reopen the folder in a container*
- If not, press **Ctrl+Shift+p** and execute: ``Remote-Containers: Reopen in Container``
- Please wait the container to be downloaded and built (usually some minutes in the first time)
- After the window is fully loaded, execute ``./start-api`` in the editor terminal to run the api

## Additional information

You can access the README files inside the [api/](https://gitlab.com/nied/AVA-Offline/tree/main/api) and [webapp/](https://gitlab.com/nied/AVA-Offline/tree/main/webapp) folders and the [project wiki](https://gitlab.com/nied/AVA-Offline/wikis/home) for more information.

There is also a guide written using the CodeTour extension on VS Code that can help you to follow the code more easily. Just search install the extension and the guides will appear in the left panel, below the file list.
