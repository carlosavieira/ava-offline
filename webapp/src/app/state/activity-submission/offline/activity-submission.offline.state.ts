import { createEntityAdapter, EntityState } from '@ngrx/entity';
import { getArrayAdapter } from '../../shared/offline/offline.state';
import { IdAndGroupId } from '../../shared/template.state';


/******************************* Created **********************************/
export interface ActivitySubmissionOfflineCreatedState extends EntityState<IdAndGroupId> {
}

export const activitySubmissionOfflineCreatedAdapter = createEntityAdapter<IdAndGroupId>();

export const activitySubmissionOfflineCreatedInitialState = activitySubmissionOfflineCreatedAdapter.getInitialState();

/******************************* Requested **********************************/
export interface ActivitySubmissionOfflineRequestedState {
  ids: EntityState<IdAndGroupId>;
  groupIds: number[];
}

export const activitySubmissionOfflineRequestedIdsAdapter = createEntityAdapter<IdAndGroupId>({
  selectId: (item) => item.id
});
export const activitySubmissionOfflineRequestedGroupIdsAdapter = getArrayAdapter<number>();


export const activitySubmissionOfflineRequestedInitialState: ActivitySubmissionOfflineRequestedState = {
  ids: activitySubmissionOfflineRequestedIdsAdapter.getInitialState(),
  groupIds: activitySubmissionOfflineRequestedGroupIdsAdapter.initialState(),
};

/******************************* Updated **********************************/
export interface ActivitySubmissionOfflineUpdatedState {
  like: ActivitySubmissionOfflineUpdatedLikeState,
  pin: ActivitySubmissionOfflineUpdatedPinState
}

export interface ActivitySubmissionOfflineUpdatedPinState {
  ids: ActivitySubmissionOfflineUpdatedPinIdState,
  indirectChanges: ActivitySubmissionOfflineUpdatedPinIndirectState
}

export interface ActivitySubmissionOfflineUpdatedLikeState extends EntityState<IdAndGroupId> {
}

export const activitySubmissionOfflineUpdatedLikeAdapter = createEntityAdapter<IdAndGroupId>({
  selectId: (item) => item.id
});

export interface ActivitySubmissionOfflineUpdatedPinIdState extends EntityState<IdAndGroupId> {
}

export const activitySubmissionOfflineUpdatedPinIdAdapter = createEntityAdapter<IdAndGroupId>({
  selectId: (item) => item.id
});

export interface ActivitySubmissionOfflineUpdatedPinIndirectState extends EntityState<IdAndGroupId> {
}

export const activitySubmissionOfflineUpdatedPinIndirectAdapter = createEntityAdapter<IdAndGroupId>({
  selectId: (item) => item.groupId,
});


export const activitySubmissionOfflineUpdatedPinInitialState: ActivitySubmissionOfflineUpdatedPinState = {
  ids: activitySubmissionOfflineUpdatedPinIdAdapter.getInitialState(),
  indirectChanges: activitySubmissionOfflineUpdatedPinIndirectAdapter.getInitialState(),
};

export const activitySubmissionOfflineUpdatedInitialState: ActivitySubmissionOfflineUpdatedState = {
  like: activitySubmissionOfflineUpdatedLikeAdapter.getInitialState(),
  pin: activitySubmissionOfflineUpdatedPinInitialState,
};


/******************************* Deleted **********************************/
export interface ActivitySubmissionOfflineDeletedState extends EntityState<IdAndGroupId> {
}

export const activitySubmissionOfflineDeletedAdapter = createEntityAdapter<IdAndGroupId>({
  selectId: (item) => item.id
});

export const activitySubmissionOfflineDeletedInitialState: ActivitySubmissionOfflineDeletedState = activitySubmissionOfflineDeletedAdapter.getInitialState();
/**************************************************************************/
/******************************* Offline **********************************/
/**************************************************************************/
export interface ActivitySubmissionOfflineState {
  created: ActivitySubmissionOfflineCreatedState;
  requested: ActivitySubmissionOfflineRequestedState;
  updated: ActivitySubmissionOfflineUpdatedState;
  deleted: ActivitySubmissionOfflineDeletedState;
}

export const activitySubmissionOfflineInitialState: ActivitySubmissionOfflineState = {
  created: activitySubmissionOfflineCreatedInitialState,
  requested: activitySubmissionOfflineRequestedInitialState,
  updated: activitySubmissionOfflineUpdatedInitialState,
  deleted: activitySubmissionOfflineDeletedInitialState
};
