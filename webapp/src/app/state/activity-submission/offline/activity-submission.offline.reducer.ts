import { combineReducers } from '@ngrx/store';
import { arrayReducer, basicReducer, idAndGroupReducer } from 'src/app/state/shared/template.reducers';
import { IdAndGroupId } from '../../shared/template.state';
import { activitySubmissionOfflineCreatedAdapter, activitySubmissionOfflineDeletedAdapter, activitySubmissionOfflineRequestedIdsAdapter, ActivitySubmissionOfflineState, activitySubmissionOfflineUpdatedLikeAdapter, activitySubmissionOfflineUpdatedPinIdAdapter, activitySubmissionOfflineUpdatedPinIndirectAdapter } from './activity-submission.offline.state';

export const activitySubmissionOfflineReducer = combineReducers<ActivitySubmissionOfflineState>({
  created: basicReducer<IdAndGroupId>('ActivitySubmission / Offline / Created', activitySubmissionOfflineCreatedAdapter),
  requested: combineReducers({
    ids: basicReducer<IdAndGroupId>('ActivitySubmission / Offline / Requested / Ids', activitySubmissionOfflineRequestedIdsAdapter),
    groupIds: arrayReducer<number>('ActivitySubmission / Offline / Requested / GroupIds')
  }),
  updated: combineReducers({
    like: idAndGroupReducer('ActivitySubmission / Offline / Updated / Like', activitySubmissionOfflineUpdatedLikeAdapter),
    pin: combineReducers({
      ids: idAndGroupReducer('ActivitySubmission / Offline / Updated / Pin / Id', activitySubmissionOfflineUpdatedPinIdAdapter),
      indirectChanges: idAndGroupReducer('ActivitySubmission / Offline / Updated / Pin / Id / Indirect', activitySubmissionOfflineUpdatedPinIndirectAdapter),
    })
  }),
  deleted: idAndGroupReducer('ActivitySubmission / Offline / Deleted', activitySubmissionOfflineDeletedAdapter),
});
