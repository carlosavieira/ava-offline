import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { of } from 'rxjs';
import { concatMap, filter, map, take, withLatestFrom } from 'rxjs/operators';
import { ActivitySubmissionSelectors } from 'src/app/state/activity-submission/activity-submission.selector';
import { LoginSelectors } from '../../login/login.selector';
import { OfflineRequestType } from '../../shared/offline/offline.state';
import { IdAndGroupId } from '../../shared/template.state';
import { AppState } from '../../state';
import { ActivitySubmissionActions } from '../activity-submission.actions';
import { ActivitySubmissionOfflineSelectors } from './activity-submission.offline.selector';

@Injectable()
export class ActivitySubmissionOfflineEffects {

  //IDK
  createOffline$ = createEffect(() => this.actions$.pipe(
    ofType(ActivitySubmissionActions.api.byCourse.id.create.offlineError),
    map(({ input, info }) => ActivitySubmissionActions.offline.created.add.one({
      data: {
        groupId: input.courseId,
        id: info.id
      }
    }))
  ));

  deleteOffline$ = createEffect(() => this.actions$.pipe(
    ofType(ActivitySubmissionActions.api.byCourse.id.delete.offlineError),
    map(({ input, info }) => ActivitySubmissionActions.offline.deleted.add({
      data: {
        groupId: input.courseId,
        id: input.id
      }
    }))
  ));

  likeOffline$ = createEffect(() => this.actions$.pipe(
    ofType(ActivitySubmissionActions.api.byCourse.id.like.offlineError),
    withLatestFrom(this.store.select(ActivitySubmissionOfflineSelectors.updated.like.state)),
    map(([{ input }, likeState]) => {
      const alreadyLikedOffline = (<number[]>likeState.ids).includes(input.id);

      if (alreadyLikedOffline) {
        return ActivitySubmissionActions.offline.updated.like.remove({
          data: {
            groupId: input.courseId,
            id: input.id
          }
        })
      }

      return ActivitySubmissionActions.offline.updated.like.add({
        data: {
          groupId: input.courseId,
          id: input.id
        }
      })
    })
  ));

  pinOffline$ = createEffect(() => this.actions$.pipe(
    ofType(ActivitySubmissionActions.api.byCourse.id.pin.offlineError),
    withLatestFrom(this.store.select(ActivitySubmissionOfflineSelectors.updated.pin.state)),
    concatMap(([{ input }, pinState]) => this.store.select(ActivitySubmissionSelectors.byCourse.id.pinned(input.courseId)).pipe(
      take(1),
      map(fixedSubmission => {
        return { input, pinState, fixedSubmission }
      })
    )),
    map(({ input, pinState, fixedSubmission }) => {
      const addItem = ActivitySubmissionActions.offline.updated.pin.ids.add({
        data: {
          groupId: input.courseId,
          id: input.id
        }
      })

      const removeItem = ActivitySubmissionActions.offline.updated.pin.ids.remove({
        data: {
          groupId: input.courseId,
          id: input.id
        }
      })

      const removeFixed = ActivitySubmissionActions.offline.updated.pin.ids.remove({
        data: {
          groupId: input.courseId,
          id: fixedSubmission?.id
        }
      })

      const addIndirect = ActivitySubmissionActions.offline.updated.pin.indirectChanges.add({
        data: {
          groupId: input.courseId,
          id: fixedSubmission?.id
        }
      })

      const removeIndirect = ActivitySubmissionActions.offline.updated.pin.indirectChanges.remove({
        data: pinState.indirectChanges.entities[input.courseId]
      })

      const alreadyModified = (<number[]>pinState.ids.ids).includes(input.id)
      const hasRelatedIndirect = (<number[]>pinState.indirectChanges.ids).includes(input.courseId);

      console.log({ hasRelatedIndirect, alreadyModified, input, fixedSubmission, pinState })

      // If true, the currently pinned submission is so "offline" only
      if (hasRelatedIndirect) {
        if (alreadyModified) { // So we will undo it
          return [removeItem, removeIndirect]
        }

        return [removeFixed, addItem]
      }

      if (alreadyModified) {
        return [removeItem]
      }

      if (fixedSubmission?.id && input.to) {
        return [addIndirect, addItem]
      }

      return [addItem]
    }),
    concatMap(actions => of(...actions))
  ));


  // ------------------------------------------------
  // SYNC
  // ------------------------------------------------

  // Todo: Coordinate this with other entities
  autoSync$ = createEffect(() => this.store.select(LoginSelectors.isOffline).pipe(
    filter(isOffline => !isOffline),
    withLatestFrom(this.store.select(ActivitySubmissionOfflineSelectors.nextAction)),
    filter(([_, type]) => type !== OfflineRequestType.None),
    map(_ => ActivitySubmissionActions.offline.sync.syncAll())
  ))


  syncAll$ = createEffect(() => this.actions$.pipe(
    ofType(ActivitySubmissionActions.offline.sync.syncAll),
    concatMap(_ => of(
      ActivitySubmissionActions.offline.sync.created.syncAll(),
      ActivitySubmissionActions.offline.sync.requested.syncAll(),
      ActivitySubmissionActions.offline.sync.updated.syncAll(),
      ActivitySubmissionActions.offline.sync.deleted.syncAll(),
    ))
  ));

  // --------------------------------------------------------------
  //  Created

  syncCreatedSyncAll$ = createEffect(() => this.actions$.pipe(
    ofType(ActivitySubmissionActions.offline.sync.created.syncAll),
    concatMap(_ => this.store.select(ActivitySubmissionOfflineSelectors.created.ids).pipe(
      take(1),
      concatMap((ids) => of(...ids).pipe(
        map((id) => ActivitySubmissionActions.offline.sync.created.byId({ input: id }))
      )),
    ))
  ));

  syncCreatedById$ = createEffect(() => this.actions$.pipe(
    ofType(ActivitySubmissionActions.offline.sync.created.byId),
    concatMap(({ input: { id, groupId } }) => this.store.select(ActivitySubmissionSelectors.byId(id)).pipe(
      take(1),
      map((activitySubmission) => {
        return [
          ActivitySubmissionActions.basic.remove.one({ data: id }),
          ActivitySubmissionActions.offline.created.remove.one({ data: id }),
          ActivitySubmissionActions.api.byCourse.id.create.request({ input: { courseId: groupId, body: activitySubmission } })
        ];
      }),
      concatMap(actions => of(...actions))
    ))
  ));

  // --------------------------------------------------------------
  //  Requested

  syncRequestedSyncAll$ = createEffect(() => this.actions$.pipe(
    ofType(ActivitySubmissionActions.offline.sync.requested.syncAll),
    concatMap(_ => of(
      ActivitySubmissionActions.offline.sync.requested.ids(),
      ActivitySubmissionActions.offline.sync.requested.groups(),
    ))
  ));


  syncRequestedIds$ = createEffect(() => this.actions$.pipe(
    ofType(ActivitySubmissionActions.offline.sync.requested.ids),
    concatMap((_) => this.store.select(ActivitySubmissionOfflineSelectors.requested.ids).pipe(
      take(1),
      concatMap(ids => of(...ids).pipe(
        map(id => ActivitySubmissionActions.offline.sync.requested.byId({ input: id }))),
      ),
    ))
  ));


  syncRequestedById$ = createEffect(() => this.actions$.pipe(
    ofType(ActivitySubmissionActions.offline.sync.requested.byId),
    map(({ input }) => [
      ActivitySubmissionActions.offline.requested.ids.remove.one({ data: input.id }),
      ActivitySubmissionActions.api.byCourse.id.get.one.request({
        input: {
          id: input.id,
          courseId: input.groupId
        }
      })
    ]),
    concatMap(actions => of(...actions))
  ));


  syncRequestedGroups$ = createEffect(() => this.actions$.pipe(
    ofType(ActivitySubmissionActions.offline.sync.requested.groups),
    concatMap((_) => this.store.select(ActivitySubmissionOfflineSelectors.requested.groups).pipe(
      take(1),
      concatMap(groups => of(...groups).pipe(
        map(groupId => ActivitySubmissionActions.offline.sync.requested.groupById({ groupId }))),
      ),
    ))
  ));

  syncRequestedGroupById$ = createEffect(() => this.actions$.pipe(
    ofType(ActivitySubmissionActions.offline.sync.requested.groupById),
    map(({ groupId }) => [
      ActivitySubmissionActions.offline.requested.groupIds.remove.one({ data: groupId }),
      ActivitySubmissionActions.api.byCourse.id.get.all.request({ input: { courseId: groupId } })
    ]),
    concatMap(actions => of(...actions))
  ));

  // --------------------------------------------------------------
  //  Updated

  syncUpdatedSyncAll$ = createEffect(() => this.actions$.pipe(
    ofType(ActivitySubmissionActions.offline.sync.updated.syncAll),
    concatMap(_ => of(
      ActivitySubmissionActions.offline.sync.updated.like.syncAll(),
      ActivitySubmissionActions.offline.sync.updated.pin.syncAll(),
    ))
  ));


  syncUpdatedLikeIds$ = createEffect(() => this.actions$.pipe(
    ofType(ActivitySubmissionActions.offline.sync.updated.like.syncAll),
    concatMap((_) => this.store.select(ActivitySubmissionOfflineSelectors.updated.like.ids).pipe(
      take(1),
      concatMap(ids => of(...ids).pipe(
        map(id => ActivitySubmissionActions.offline.sync.updated.like.byId({ input: id }))),
      ),
    ))
  ));


  syncUpdatedLikeById$ = createEffect(() => this.actions$.pipe(
    ofType(ActivitySubmissionActions.offline.sync.updated.like.byId),
    concatMap(({ input }) => this.store.select(ActivitySubmissionSelectors.byId(input.id)).pipe(
      take(1),
      map(entity => {
        return {
          idAndGroup: input,
          requestData: {
            courseId: input.groupId,
            id: input.id,
            to: entity.liked
          }
        }
      })
    )),
    map(({ idAndGroup, requestData }) => [
      ActivitySubmissionActions.offline.updated.like.remove({ data: idAndGroup }),
      ActivitySubmissionActions.api.byCourse.id.like.request({ input: requestData })
    ]),
    concatMap(actions => of(...actions))
  ));

  syncUpdatedPin$ = createEffect(() => this.actions$.pipe(
    ofType(ActivitySubmissionActions.offline.sync.updated.pin.syncAll),
    withLatestFrom(this.store.select(ActivitySubmissionOfflineSelectors.updated.pin.ids.list)),
    concatMap(([_, ids]) => of(...new Set(ids.map(item => item.groupId))).pipe(
      map(courseId => {
        const myIds = ids.filter(id => id.groupId === courseId);
        return ActivitySubmissionActions.offline.sync.updated.pin.byCourse({ courseId, ids: myIds });
      })
    )),
  ));


  syncUpdatedPinByCourse$ = createEffect(() => this.actions$.pipe(
    ofType(ActivitySubmissionActions.offline.sync.updated.pin.byCourse),
    concatMap(({ courseId, ids }) => this.store.select(ActivitySubmissionSelectors.byCourse.id.pinned(courseId)).pipe(
      take(1),
      withLatestFrom(this.store.select(ActivitySubmissionOfflineSelectors.updated.pin.indirect.state)),
      map(([pinnedSubmission, indirectState]) => {
        const indirectId = indirectState.entities[courseId]

        return { pinnedSubmission, indirectId, ids }
      })
    )),
    map(({ ids, pinnedSubmission, indirectId }) => {
      let item: IdAndGroupId;
      let to: boolean;

      if (ids.length > 1) {
        to = true
        if (ids[1].id === pinnedSubmission?.id) {
          item = ids[0]
        } else {
          item = ids[1]
        }
      } else {
        item = ids[0];
        to = item.id === pinnedSubmission?.id
      }

      return { ids, item, to, indirectId }
    }),
    map(({ item, to, ids, indirectId }) => [
      !indirectId ? null : ActivitySubmissionActions.offline.updated.pin.indirectChanges.remove({ data: indirectId }),
      ...ids.map(id => ActivitySubmissionActions.offline.updated.pin.ids.remove({ data: id })),
      ActivitySubmissionActions.api.byCourse.id.pin.request({
        input: {
          courseId: item.groupId,
          id: item.id,
          to
        }
      })
    ]),
    concatMap(actions => of(...actions)),
    filter(action => action !== null)
  ));

  // --------------------------------------------------------------
  //  Deleted

  syncDeletedSyncAll$ = createEffect(() => this.actions$.pipe(
    ofType(ActivitySubmissionActions.offline.sync.deleted.syncAll),
    concatMap(_ => this.store.select(ActivitySubmissionOfflineSelectors.deleted.ids).pipe(
      take(1),
      concatMap((ids) => of(...ids).pipe(
        map((input) => ActivitySubmissionActions.offline.sync.deleted.byId({ input }))
      )),
    ))
  ));

  syncDeletedById$ = createEffect(() => this.actions$.pipe(
    ofType(ActivitySubmissionActions.offline.sync.deleted.byId),
    map(({ input: { groupId, id } }) => [
      ActivitySubmissionActions.offline.deleted.remove({ data: { groupId, id } }),
      ActivitySubmissionActions.api.byCourse.id.delete.request({ input: { courseId: groupId, id } })
    ]),
    concatMap(actions => of(...actions))
  ));

  constructor(private actions$: Actions, private store: Store<AppState>) { }
}
