import { Action, createReducer, on } from '@ngrx/store';
import { fromArray } from 'src/app/models';
import { ActivitySubmissionSM, fromJsonToActivitySubmissionSM } from 'src/app/models/activity-submission.model';
import { GradesFinalActions } from '../grades-final/grades-final.actions';
import { LoginActions } from '../login/login.actions';
import { nowString } from '../shared';
import { Metadata, metadataAdapter, MetadataType } from '../shared/metadata/metadata';
import { basicReducer, joinReducers } from '../shared/template.reducers';
import { ActivitySubmissionActions } from './activity-submission.actions';
import { activitySubmissionAdapter as adapter, ActivitySubmissionInitialState as initialState, ActivitySubmissionState, ActivitySubmissionState as State } from './activity-submission.state';
import { activitySubmissionOfflineReducer } from './offline/activity-submission.offline.reducer';

const reducer = createReducer(
  initialState,
  on(LoginActions.clear, (state) => {
    return initialState
  }),
  on(ActivitySubmissionActions.getMine.success, (state, { data }) => {
    if (!data) {
      return state;
    }

    const dataSM: ActivitySubmissionSM = fromJsonToActivitySubmissionSM(data);
    const newState = adapter.upsertOne(dataSM, state);

    const newMetadata: Metadata<number> = ({ id: data.id, lastUpdate: nowString(), type: MetadataType.Item });
    const stateMetadataUpdated = metadataAdapter<number>().upsertOne(newMetadata, state.metadata);

    return { ...newState, metadata: stateMetadataUpdated };
  }),
  on(GradesFinalActions.getUserOverview.success, (state, { data, input }) => {
    const dataSM: ActivitySubmissionSM[] = fromArray(fromJsonToActivitySubmissionSM, data.grades.map(grade => grade.activitySubmission));
    const newState = adapter.upsertMany(dataSM, state);

    const newMetadata: Metadata<number>[] = dataSM.map(item => ({ id: item.id, lastUpdate: nowString(), type: MetadataType.Item }));
    const stateMetadataUpdated = metadataAdapter<number>().upsertMany(newMetadata, state.metadata);

    return { ...newState, metadata: stateMetadataUpdated };
  }),
  on(ActivitySubmissionActions.getAll.success, (state, { data }) => {
    const dataSM: ActivitySubmissionSM[] = fromArray(fromJsonToActivitySubmissionSM, data);
    const newState = adapter.upsertMany(dataSM, state);

    const newMetadata: Metadata<number>[] = data.map(item => ({ id: item.id, lastUpdate: nowString(), type: MetadataType.Item }));
    const stateMetadataUpdated = metadataAdapter<number>().upsertMany(newMetadata, state.metadata);

    return { ...newState, metadata: stateMetadataUpdated };
  }),
  on(ActivitySubmissionActions.create.success, (state, { data }) => {
    const dataSM: ActivitySubmissionSM = fromJsonToActivitySubmissionSM(data);
    const newState = adapter.upsertOne(dataSM, state);

    const newMetadata: Metadata<number> = ({ id: data.id, lastUpdate: nowString(), type: MetadataType.Item });
    const stateMetadataUpdated = metadataAdapter<number>().upsertOne(newMetadata, state.metadata);

    return { ...newState, metadata: stateMetadataUpdated };
  }),
  on(ActivitySubmissionActions.edit.success, (state, { data }) => {
    const dataSM: ActivitySubmissionSM = fromJsonToActivitySubmissionSM(data);
    const newState = adapter.upsertOne(dataSM, state);

    const newMetadata: Metadata<number> = ({ id: data.id, lastUpdate: nowString(), type: MetadataType.Item });
    const stateMetadataUpdated = metadataAdapter<number>().upsertOne(newMetadata, state.metadata);

    return { ...newState, metadata: stateMetadataUpdated };
  }),
  on(ActivitySubmissionActions.delete.success, (state, { input: { submissionId } }) => {
    const newState = adapter.removeOne(submissionId, state);

    const stateMetadataUpdated = metadataAdapter<number>().removeOne(`${MetadataType.Item}/${submissionId}`, state.metadata);

    return { ...newState, metadata: stateMetadataUpdated };
  }),
  on(ActivitySubmissionActions.keyLoaded, (state, { data }) => {
    return data ?? state;
  })
);

export function ActivitySubmissionReducer(state: State | undefined, action: Action) {
  return joinReducers<ActivitySubmissionSM, ActivitySubmissionState>(state, action, [
    reducer,
    basicReducer('ActivitySubmission', adapter),
    (myState: State, myAction: Action) => {
      return {
        ...myState,
        offline: activitySubmissionOfflineReducer(myState.offline, myAction)
      };
    }
  ]);
}
