import { ActivitySubmissionForm, ActivitySubmissionJson, ActivitySubmissionSM } from "src/app/models/activity-submission.model";
import { ActionTemplates } from '../shared/template.actions';
import { ActivitySubmissionState } from './activity-submission.state';


export const ActivitySubmissionActions = {
  keyLoaded: ActionTemplates.keyLoaded<ActivitySubmissionState>('ActivitySubmission'),

  getMine: ActionTemplates.validated.withArgs<{ courseId: number, activityId: number }, ActivitySubmissionJson>('[ ActivitySubmission / API ] Get my submission'),
  getAll: ActionTemplates.validated.withArgs<{ courseId: number, activityId: number }, ActivitySubmissionJson[]>('[ ActivitySubmission / API ] Get all submissions'),
  create: ActionTemplates.validated.withArgs<{ form: ActivitySubmissionForm, courseId: number, activityId: number, userId: number }, ActivitySubmissionJson>('[ ActivitySubmission / API ] Create an submission'),
  edit: ActionTemplates.validated.withArgs<{ form: ActivitySubmissionForm, courseId: number, activityId: number, userId: number }, ActivitySubmissionJson>('[ ActivitySubmission / API ] Edit an submission'),
  delete: ActionTemplates.validated.withArgs<{ courseId: number, activityId: number, submissionId: number }, void>('[ ActivitySubmission / API ] Delete an submission'),

  basic: ActionTemplates.basicActions<ActivitySubmissionSM, number>('ActivitySubmission'),
};
