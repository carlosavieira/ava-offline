import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { of } from 'rxjs';
import { catchError, concatMap, map } from 'rxjs/operators';
import { ActivityItemSM } from 'src/app/models/activity-item.model';
import { ActivitiesApiService } from 'src/app/services/api/activities.api.service';
import { ActivityItemActions } from '../activity-item/activity-item.actions';
import { ActivitySubmissionActions } from './activity-submission.actions';

@Injectable()
export class ActivitySubmissionEffects {

  getMine$ = createEffect(() => this.actions$.pipe(
    ofType(ActivitySubmissionActions.getMine.request),
    concatMap(({ input }) => this.activitiesApiService.getMySubmission(input).pipe(
      map(submission => [
        ActivitySubmissionActions.getMine.success({ input, data: submission }),
        ActivityItemActions.indirectlyUpsert({
          items: [
            {
              activityId: input.activityId,
              submissionId: submission.id,
              userId: submission.lastModifiedBy.id,
              evaluationId: undefined
            } as ActivityItemSM
          ]
        })
      ]),
      catchError((error: any) => of([
        ActivitySubmissionActions.getMine.error({ input, error })
      ]))
    )),
    concatMap(actions => of(...actions))
  ));

  getAll$ = createEffect(() => this.actions$.pipe(
    ofType(ActivitySubmissionActions.getAll.request),
    concatMap(({ input }) => this.activitiesApiService.getSubmissions(input).pipe(
      map(submissions => ActivitySubmissionActions.getAll.success({ input, data: submissions })),
      catchError((error: any) => of(ActivitySubmissionActions.getAll.error({ input, error })))
    ))
  ));

  create$ = createEffect(() => this.actions$.pipe(
    ofType(ActivitySubmissionActions.create.request),
    concatMap(({ input }) => this.activitiesApiService.createSubmission(input).pipe(
      map(submission => ActivitySubmissionActions.create.success({ input, data: submission })),
      catchError((error: any) => of(ActivitySubmissionActions.create.error({ input, error })))
    ))
  ));


  edit$ = createEffect(() => this.actions$.pipe(
    ofType(ActivitySubmissionActions.edit.request),
    concatMap(({ input }) => this.activitiesApiService.editSubmission(input).pipe(
      map(submission => ActivitySubmissionActions.edit.success({ input, data: submission })),
      catchError((error: any) => of(ActivitySubmissionActions.edit.error({ input, error })))
    ))
  ));

  delete$ = createEffect(() => this.actions$.pipe(
    ofType(ActivitySubmissionActions.delete.request),
    concatMap(({ input }) => this.activitiesApiService.deleteSubmission(input).pipe(
      map(_ => ActivitySubmissionActions.delete.success({ input, data: null })),
      catchError((error: any) => of(ActivitySubmissionActions.delete.error({ input, error })))
    ))
  ));


  constructor(private actions$: Actions, private activitiesApiService: ActivitiesApiService, private store$: Store) { }
}
