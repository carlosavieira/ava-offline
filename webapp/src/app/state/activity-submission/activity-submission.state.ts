import { createEntityAdapter, EntityAdapter, EntityState } from '@ngrx/entity';
import { ActivitySubmissionSM } from 'src/app/models/activity-submission.model';
import { metadataAdapter, MetadataState } from '../shared/metadata/metadata';
import { activitySubmissionOfflineInitialState, ActivitySubmissionOfflineState } from './offline/activity-submission.offline.state';

export interface ActivitySubmissionState extends EntityState<ActivitySubmissionSM> {
  metadata: MetadataState<number>;
  offline: ActivitySubmissionOfflineState;
}

export const activitySubmissionAdapter: EntityAdapter<ActivitySubmissionSM> = createEntityAdapter<ActivitySubmissionSM>();

export const ActivitySubmissionInitialState: ActivitySubmissionState = activitySubmissionAdapter.getInitialState({
  metadata: metadataAdapter<number>().getInitialState(),
  offline: activitySubmissionOfflineInitialState,
});
