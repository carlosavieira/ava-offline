import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { catchError, concatMap, map } from 'rxjs/operators';
import { UserApiService } from './../../services/api/user.api.service';
import { UserActions } from './user.actions';

@Injectable()
export class UserEffects {

  load$ = createEffect(() => this.actions$.pipe(
    ofType(UserActions.fetchAll.request),
    concatMap(_ => this.userApiService.getUsers().pipe(
      map(users => UserActions.fetchAll.success({ data: users })),
      catchError((error: any) => of(UserActions.fetchAll.error({ error })))
    ))
  ));

  constructor(private actions$: Actions, private userApiService: UserApiService) { }
}
