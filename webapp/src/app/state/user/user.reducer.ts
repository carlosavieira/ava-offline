import { Action, createReducer, on } from '@ngrx/store';
import { LoginActions } from '../login/login.actions';
import { basicReducer, joinReducers } from '../shared/template.reducers';
import { UserActions } from './user.actions';
import { userAdapter as adapter, UserInitialState as initialState, UserState as State } from './user.state';

const userReducer = createReducer(
  initialState,
  on(LoginActions.clear, (state) => {
    return initialState
  }),
  on(UserActions.select, (state, { user }) => {
    return { ...state, selectedUserId: user };
  }),
  on(UserActions.keyLoaded, (state, { data }) => {
    return data ?? state;
  })
);

export function UserReducer(state: State | undefined, action: Action) {
  // const reducers = {
  //   custom: userReducer,
  //   basic: idReducer(initialState, 'User', adapter)
  // };

  // return combineReducers(reducers)(state, action);

  return joinReducers(state, action, [
    userReducer,
    basicReducer('User', adapter)
  ]);

}
