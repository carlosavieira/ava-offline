import { createEntityAdapter, EntityAdapter, EntityState } from '@ngrx/entity';
import { UserSM } from 'src/app/models/user.model';

export interface UserState extends EntityState<UserSM> { }

export const userAdapter: EntityAdapter<UserSM> = createEntityAdapter<UserSM>();

export const UserInitialState: UserState = userAdapter.getInitialState();
