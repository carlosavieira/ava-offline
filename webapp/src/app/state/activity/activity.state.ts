import { createEntityAdapter, EntityAdapter, EntityState } from '@ngrx/entity';
import { ActivityFilter, ActivitySM } from 'src/app/models/activity.model';
import { groupAdapter, GroupState } from '../shared/group/group';
import { metadataAdapter, MetadataState } from '../shared/metadata/metadata';

export interface ActivityState extends EntityState<ActivitySM> {
  selectedActivityId: number | null;
  filter: ActivityFilter;
  groups: GroupState;
  metadata: MetadataState<number>;
}

export const activityAdapter: EntityAdapter<ActivitySM> = createEntityAdapter<ActivitySM>();

export const ActivityInitialState: ActivityState = activityAdapter.getInitialState({
  selectedActivityId: null,
  filter: ActivityFilter.NoFilter,
  groups: groupAdapter.getInitialState(),
  metadata: metadataAdapter<number>().getInitialState(),
});
