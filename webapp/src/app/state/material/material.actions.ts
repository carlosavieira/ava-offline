import { MaterialForm, MaterialJson, MaterialSM } from 'src/app/models/material.model';
import { ActionTemplates } from '../shared/template.actions';
import { MaterialState } from './material.state';
import { MaterialOfflineActions } from './offline/material.offline.actions';


export const MaterialActions = {
  keyLoaded: ActionTemplates.keyLoaded<MaterialState>('Material'),

  fetchAll: ActionTemplates.validated.withArgs<{ courseId: number }, MaterialJson[]>('[ Material / API ] Load all materials from a course'),

  create: {
    files: ActionTemplates.validated.withArgs<{ files: File[], courseId: number }, MaterialJson[]>('[ Material / API ] Create a file'),
    link: ActionTemplates.validated.withArgs<{ body: MaterialForm, courseId: number }, MaterialJson>('[ Material / API ] Create a link'),
  },
  editLink: ActionTemplates.validated.withArgs<{ body: MaterialForm, materialId: number, courseId: number }, MaterialJson>('[ Material / API ] Edit material link'),
  delete: ActionTemplates.validated.withArgs<{ materialId: number, courseId: number }, void>('[ Material / API ] Delete a material'),

  offline: MaterialOfflineActions,
  basic: ActionTemplates.basicActions<MaterialSM>('Material'),
};
