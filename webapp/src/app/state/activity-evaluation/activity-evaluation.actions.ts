import { ActivityEvaluationForm, ActivityEvaluationJson, ActivityEvaluationSM } from "src/app/models/activity-evaluation.model";
import { ActionTemplates } from '../shared/template.actions';
import { ActivityEvaluationState } from './activity-evaluation.state';


export const ActivityEvaluationActions = {
  keyLoaded: ActionTemplates.keyLoaded<ActivityEvaluationState>('ActivityEvaluation'),

  get: ActionTemplates.validated.withArgs<{ courseId: number, activityId: number, submissionId: number }, ActivityEvaluationJson>('[ ActivityEvaluation / API ] Get an evaluation'),
  create: ActionTemplates.validated.withArgs<{ form: ActivityEvaluationForm, courseId: number, activityId: number, userId: number }, ActivityEvaluationJson>('[ ActivityEvaluation / API ] Create an evaluation'),
  edit: ActionTemplates.validated.withArgs<{ form: ActivityEvaluationForm, courseId: number, activityId: number, userId: number }, ActivityEvaluationJson>('[ ActivityEvaluation / API ] Edit an evaluation'),

  basic: ActionTemplates.basicActions<ActivityEvaluationSM, number>('ActivityEvaluation'),
};
