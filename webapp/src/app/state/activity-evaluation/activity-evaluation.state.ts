import { createEntityAdapter, EntityAdapter, EntityState } from '@ngrx/entity';
import { ActivityEvaluationSM } from 'src/app/models/activity-evaluation.model';
import { metadataAdapter, MetadataState } from '../shared/metadata/metadata';

export interface ActivityEvaluationState extends EntityState<ActivityEvaluationSM> {
  metadata: MetadataState<number>;
}

export const activityEvaluationAdapter: EntityAdapter<ActivityEvaluationSM> = createEntityAdapter<ActivityEvaluationSM>();

export const ActivityEvaluationInitialState: ActivityEvaluationState = activityEvaluationAdapter.getInitialState({
  metadata: metadataAdapter<number>().getInitialState(),
});
