import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { of } from 'rxjs';
import { catchError, concatMap, map } from 'rxjs/operators';
import { ActivitiesApiService } from 'src/app/services/api/activities.api.service';
import { ActivityEvaluationActions } from './activity-evaluation.actions';

@Injectable()
export class ActivityEvaluationEffects {

  get$ = createEffect(() => this.actions$.pipe(
    ofType(ActivityEvaluationActions.get.request),
    concatMap(({ input }) => this.activitiesApiService.getEvaluation(input).pipe(
      map(evaluation => ActivityEvaluationActions.get.success({ input, data: evaluation })),
      catchError((error: any) => of(ActivityEvaluationActions.get.error({ input, error })))
    ))
  ));


  create$ = createEffect(() => this.actions$.pipe(
    ofType(ActivityEvaluationActions.create.request),
    concatMap(({ input }) => this.activitiesApiService.createEvaluation(input).pipe(
      map(evaluation => ActivityEvaluationActions.create.success({ input, data: evaluation })),
      catchError((error: any) => of(ActivityEvaluationActions.create.error({ input, error })))
    ))
  ));


  edit$ = createEffect(() => this.actions$.pipe(
    ofType(ActivityEvaluationActions.edit.request),
    concatMap(({ input }) => this.activitiesApiService.editEvaluation(input).pipe(
      map(evaluation => ActivityEvaluationActions.edit.success({ input, data: evaluation })),
      catchError((error: any) => of(ActivityEvaluationActions.edit.error({ input, error })))
    ))
  ));

  constructor(private actions$: Actions, private activitiesApiService: ActivitiesApiService, private store$: Store) { }
}
