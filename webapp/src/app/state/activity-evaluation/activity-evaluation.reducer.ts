import { Action, createReducer, on } from '@ngrx/store';
import { fromArray } from 'src/app/models';
import { ActivityEvaluationSM, fromJsonToActivityEvaluationSM } from 'src/app/models/activity-evaluation.model';
import { GradesFinalActions } from '../grades-final/grades-final.actions';
import { LoginActions } from '../login/login.actions';
import { nowString } from '../shared';
import { Metadata, metadataAdapter, MetadataType } from '../shared/metadata/metadata';
import { ActivityEvaluationActions } from './activity-evaluation.actions';
import { activityEvaluationAdapter as adapter, ActivityEvaluationInitialState as initialState, ActivityEvaluationState as State } from './activity-evaluation.state';

const reducer = createReducer(
  initialState,
  on(LoginActions.clear, (state) => {
    return initialState
  }),
  on(GradesFinalActions.getUserOverview.success, (state, { data, input }) => {
    const dataSM: ActivityEvaluationSM[] = fromArray(
      fromJsonToActivityEvaluationSM,
      data.grades.map(grade =>
        grade?.activityEvaluation
      ).filter(item => item?.id)
    );
    const newState = adapter.upsertMany(dataSM, state);

    const newMetadata: Metadata<number>[] = dataSM.map(item => ({ id: item.id, lastUpdate: nowString(), type: MetadataType.Item }));
    const stateMetadataUpdated = metadataAdapter<number>().upsertMany(newMetadata, state.metadata);

    return { ...newState, metadata: stateMetadataUpdated };
  }),
  on(ActivityEvaluationActions.get.success, (state, { data, input: { courseId } }) => {
    const dataSM: ActivityEvaluationSM = fromJsonToActivityEvaluationSM(data);
    const newState = adapter.upsertOne(dataSM, state);

    const newMetadata: Metadata<number> = ({ id: data.id, lastUpdate: nowString(), type: MetadataType.Item });
    const stateMetadataUpdated = metadataAdapter<number>().upsertOne(newMetadata, state.metadata);

    return { ...newState, metadata: stateMetadataUpdated };
  }),
  on(ActivityEvaluationActions.create.success, (state, { input: { courseId }, data }) => {
    const dataSM: ActivityEvaluationSM = fromJsonToActivityEvaluationSM(data);
    const newState = adapter.upsertOne(dataSM, state);

    const newMetadata: Metadata<number> = ({ id: data.id, lastUpdate: nowString(), type: MetadataType.Item });
    const stateMetadataUpdated = metadataAdapter<number>().upsertOne(newMetadata, state.metadata);

    return { ...newState, metadata: stateMetadataUpdated };
  }),
  on(ActivityEvaluationActions.edit.success, (state, { input: { courseId }, data }) => {
    const dataSM: ActivityEvaluationSM = fromJsonToActivityEvaluationSM(data);
    const newState = adapter.upsertOne(dataSM, state);

    const newMetadata: Metadata<number> = ({ id: data.id, lastUpdate: nowString(), type: MetadataType.Item });
    const stateMetadataUpdated = metadataAdapter<number>().upsertOne(newMetadata, state.metadata);

    return { ...newState, metadata: stateMetadataUpdated };
  }),
  on(ActivityEvaluationActions.keyLoaded, (state, { data }) => {
    return data ?? state;
  })
);

export function ActivityEvaluationReducer(state: State | undefined, action: Action) {
  return reducer(state, action);
}

