import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { combineLatest, Observable } from 'rxjs';
import { concatMap, map } from 'rxjs/operators';
import { User } from 'src/app/models/user.model';
import { ProfileService } from '../profile.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})

export class ProfileComponent implements OnInit {
  private me$: Observable<User>;
  profile$: Observable<User>;
  canEdit$: Observable<boolean>;

  constructor(
    private profileService: ProfileService,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.me$ = this.route.data.pipe(map((data: { user: User }) => data.user))
    this.profile$ = this.route.paramMap.pipe(
      map(p => Number(p.get('profileId'))),
      concatMap(id =>
        id > 0 ?
          this.profileService.getUser(id)
          : this.me$
      ),
    );

    this.canEdit$ = combineLatest([this.me$, this.profile$]).pipe(map(([me, p]) => me?.isAdmin || me?.id === p?.id));
  }
}
