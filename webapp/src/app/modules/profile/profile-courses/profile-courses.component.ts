import { Component, Input, OnInit } from '@angular/core';
import { CourseJson } from 'src/app/models/course.model';
import { ProfileService } from '../profile.service';

@Component({
  selector: 'app-profile-courses',
  templateUrl: './profile-courses.component.html',
  styleUrls: ['./profile-courses.component.css']
})
export class ProfileCoursesComponent implements OnInit {

  @Input() userId: number;
  cursos: CourseJson[][] = [];

  translationText = ProfileService.translationText.ProfileCoursesComponent;

  constructor(private profileService: ProfileService) { }

  ngOnInit() {
    this.profileService.getCoursesAsTeacher(this.userId).subscribe((data: CourseJson[]) => {
      this.cursos.push(data);
    });

    this.profileService.getCoursesAsStudent(this.userId).subscribe((data: CourseJson[]) => {
      this.cursos.push(data);
    });
  }

}
