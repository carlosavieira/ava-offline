import { Component, Input, OnInit } from '@angular/core';
import { User } from 'src/app/models/user.model';
import { ProfileService } from '../profile.service';

@Component({
  selector: 'app-profile-personal-data',
  templateUrl: './profile-personal-data.component.html',
  styleUrls: ['./profile-personal-data.component.css']
})
export class ProfilePersonalDataComponent implements OnInit {

  @Input() profile: User;
  @Input() canEdit: boolean;
  editing = false;
  urlPhoto: string;

  translationText = ProfileService.translationText.ProfilePersonalDataComponent;

  constructor(private profileService: ProfileService) {
  }

  ngOnInit() {
    this.urlPhoto = this.profileService.getProfilePhoto(this.profile?.id);
  }
}
