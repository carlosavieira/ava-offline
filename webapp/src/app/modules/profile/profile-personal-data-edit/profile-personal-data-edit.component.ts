import { Component, EventEmitter, Input, OnInit, Output, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { base64ToFile, ImageCroppedEvent } from 'ngx-image-cropper';
import { User } from 'src/app/models/user.model';
import { ProfileService } from '../profile.service';

@Component({
  selector: 'app-profile-personal-data-edit',
  templateUrl: './profile-personal-data-edit.component.html',
  encapsulation: ViewEncapsulation.None,
  styleUrls: ['./profile-personal-data-edit.component.css']
})

export class ProfilePersonalDataEditComponent implements OnInit {

  @Input() profile: User;
  @Output() cancel = new EventEmitter<void>();

  form: FormGroup;
  urlPhoto: string;
  aboutMeNumCharsTyped: number;
  aboutMeNumCharsTypedColor: string;
  aboutMeErrorMaxNumCharsExceeded = false;

  /**
   * Max number of chars allowed for aboutMe.
   * If change, change too in src/app/dev/languages.ts => 'profile' => 'ProfilePersonalDataEditComponent'
   */
  aboutMeMaxNumChars = 1000;

  /**
   * Max number of chars allowed .
   * If change, change too in src/app/dev/languages.ts
   * => 'profile' => 'ProfilePersonalDataEditComponent'
   */
  nameMaxNumChars = 100;

  // image cropper
  fileImage: Blob;
  croppedImage: any;
  showCroppedImage = false;
  imageChangedEvent: any;

  translationText = ProfileService.translationText.ProfilePersonalDataEditComponent;

  actions = ProfileService.translationText.actions;

  constructor(private fb: FormBuilder, private profileService: ProfileService) {
  }

  ngOnInit() {

    this.form = this.fb.group({
      name: [this.profile.name, [
        Validators.required,
        Validators.maxLength(this.nameMaxNumChars)
      ]],
      aboutMe: [this.profile.aboutMe, [
        Validators.maxLength(this.aboutMeMaxNumChars)
      ]]
    });

    if (this.profile.aboutMe) {
      this.aboutMeNumCharsTyped = this.profile.aboutMe.length;
    } else {
      this.aboutMeNumCharsTyped = 0;
    }

    if (this.profile.picture) {
      this.urlPhoto = this.profileService.getProfilePhoto(this.profile?.id);
    } else {
      this.urlPhoto = null;
    }
  }

  aboutMeSetNumberChars(txt: string) {
    this.aboutMeNumCharsTyped = txt.length;
    if (this.aboutMeNumCharsTyped > this.aboutMeMaxNumChars) {
      this.aboutMeNumCharsTypedColor = 'danger';
    } else {
      this.aboutMeNumCharsTypedColor = 'primary';
    }
  }

  onSubmit() {
    console.log('updating profile...');

    this.profileService.updateProfile(this.profile?.id, this.form.value).subscribe((profile: User) => {
      if (this.showCroppedImage) {
        const file = new File([this.fileImage], 'profile.jpeg', { lastModified: (new Date()).getTime(), type: 'image/jpeg' });

        this.profileService.updateProfilePhoto(this.profile?.id, file).subscribe(_ => {
          console.log('updating photo...');
          location.reload();
        });
      } else if (!this.urlPhoto) {
        this.profileService.deleteProfilePhoto(this.profile?.id).subscribe(_ => {
          console.log('deleting photo...');
          location.reload();
        })
      } else {
        location.reload();
      }
    });


  }

  //
  // Image Cropper
  //
  fileChangeEvent(event: any): void {
    this.imageChangedEvent = event;
  }

  imageCropped(event: ImageCroppedEvent) {
    this.fileImage = base64ToFile(event.base64);

    this.croppedImage = event.base64;
  }

  imageLoaded() {
    // show cropper
  }

  cropperReady() {
    // cropper ready
  }

  loadImageFailed() {
    // show message
  }

}
