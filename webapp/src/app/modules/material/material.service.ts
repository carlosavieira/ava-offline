import { Injectable } from '@angular/core';
import { LANGUAGE } from 'src/app/dev/languages';
import { MaterialApiService } from 'src/app/services/api/material.api.service';
import { SharedService } from './../shared/shared.service';

@Injectable()
export class MaterialService {

  static translationText = LANGUAGE.material;

  constructor(private sharedService: SharedService, private api: MaterialApiService) { }

  getUrlTitle(link: string) {
    return this.api.getUrlTitle(link);
  }
}
