import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Course } from 'src/app/models/course.model';
import { Material } from 'src/app/models/material.model';
import { SharedService } from 'src/app/modules/shared/shared.service';
import { MaterialService } from '../material.service';

@Component({
  selector: 'app-material-link',
  templateUrl: './material-link.component.html',
  styleUrls: ['./material-link.component.css']
})
export class MaterialLinkComponent implements OnInit {

  @Input() material: Material;
  @Input() canDelete: boolean;

  courseId: number;
  dateReadable: string;
  dateFull: string;
  sizeStr: string;

  constructor(private sharedService: SharedService, private route: ActivatedRoute, private materialService: MaterialService) {
    this.route.data.subscribe((data: { course: Course }) => {
      this.courseId = data.course?.id;
    });
  }

  ngOnInit() {
    this.dateFull = this.material?.lastModifiedDate.toLocaleString('pt-BR');
    // this.dateReadable = this.sharedService.dateString(this.material?.lastModifiedDate);
    this.dateReadable = this.dateFull;
  }

  openModal() {
  }

  update(reloadPage: boolean) {

    if (reloadPage) {
      // this.updated.emit();
    }
  }
}
