import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { filter, map, take, tap } from 'rxjs/operators';
import { Material } from 'src/app/models/material.model';
import { CourseSelectors } from 'src/app/state/course/course.selector';
import { MaterialActions } from 'src/app/state/material/material.actions';
import { MaterialAdvancedSelectors } from 'src/app/state/material/material.advanced.selector';
import { MaterialSelectors } from 'src/app/state/material/material.selector';
import { ParticipationAdvancedSelectors } from 'src/app/state/participation/participation.advanced.selector';


@Component({
  selector: 'app-material',
  templateUrl: './material.component.html',
  styleUrls: ['./material.component.css']
})
export class MaterialComponent implements OnInit {

  materials$: Observable<Material[]>;

  canDelete$: Observable<boolean>;
  canUpload$: Observable<boolean>;

  constructor(private store: Store) {
    this.fetchAll();
    this.materials$ = this.store.select(MaterialAdvancedSelectors.sel.many(MaterialSelectors.byCourse.current.all))
  }

  ngOnInit() {
    this.canDelete$ = this.store.select(ParticipationAdvancedSelectors.hasPermission('delete_material'));
    this.canUpload$ = this.store.select(ParticipationAdvancedSelectors.hasPermission('create_material'));
  }

  fetchAll() {
    this.store.select(CourseSelectors.currentId).pipe(
      filter(courseId => !!courseId),
      take(1),
      map(courseId => Number(courseId)),
      tap((courseId) => {
        this.store.dispatch(MaterialActions.fetchAll.request({ input: { courseId } }))
      }),
    ).subscribe();
  }


  trackMaterial(index: number, material: Material) {
    return material?.id;
  }
}
