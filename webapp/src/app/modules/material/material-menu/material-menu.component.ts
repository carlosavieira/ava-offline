import { Component, EventEmitter, Input, Output } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Course } from 'src/app/models/course.model';

@Component({
  selector: 'app-material-menu',
  templateUrl: './material-menu.component.html',
  styleUrls: ['./material-menu.component.css']
})
export class MaterialMenuComponent {

  @Output() refresh = new EventEmitter<void>();
  @Input() canUpload: boolean;

  courseId: number;
  address: string;

  constructor(private route: ActivatedRoute) {
    this.route.data
      .subscribe((data: { course: Course }) => {
        this.courseId = data.course?.id;
        this.address = 'courses/' + this.courseId + '/materials/files';

        this.update();
      });
  }



  update() {
    this.refresh.emit();
    return;
  }
}
