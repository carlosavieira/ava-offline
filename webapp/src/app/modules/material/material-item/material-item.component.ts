import { Component, Input, OnChanges, OnInit } from '@angular/core';
import { IconProp } from '@fortawesome/fontawesome-svg-core';
import { Store } from '@ngrx/store';
import { take } from 'rxjs/operators';
import { FileState, FileUploaded } from 'src/app/models/file-uploaded.model';
import { Material } from 'src/app/models/material.model';
import { FileApiService } from 'src/app/services/api/file.api.service';
import { CourseSelectors } from 'src/app/state/course/course.selector';
import { getFileId } from 'src/app/state/file-uploaded/file-uploaded.state';
import { LoginAdvancedSelectors } from 'src/app/state/login/login.advanced.selector';
import { MaterialActions } from 'src/app/state/material/material.actions';
import { SharedService } from './../../shared/shared.service';


@Component({
  selector: 'app-material-item',
  templateUrl: './material-item.component.html',
  styleUrls: ['./material-item.component.css']
})
export class MaterialItemComponent implements OnInit, OnChanges {

  @Input() material: Material;
  @Input() canDelete: boolean;

  hasFile: boolean = false;
  fileClass: IconProp = 'file';
  url: string;

  courseId: number;
  dateReadable: string;
  dateFull: string = '';
  sizeStr: string;

  constructor(private sharedService: SharedService, private store: Store, private fileApi: FileApiService) {
    this.store.select(CourseSelectors.currentId).subscribe((data: number
    ) => {
      this.courseId = Number(data);
    });

    this.ngOnChanges()
  }

  ngOnInit() {
    this.dateFull = this.material?.lastModifiedDate.toLocaleString('pt-BR');
    this.dateReadable = this.dateFull;

    this.hasFile = !!this.getFile()

    if (this.hasFile) {
      this.fileClass = this.sharedService.getFileIconClass(this.getFile().mimeType);
      this.sizeStr = this.sharedService.byteSizeString(this.getFile().byteSize);
    }
  }


  ngOnChanges() {
    if (this.material?.link) {
      this.url = this.material.link;
    } else {

      this.store.select(
        LoginAdvancedSelectors.fileDownloadLink,
        { path: this.material?.files[0]?.downloadUri }
      ).pipe(take(1)).subscribe(url => this.url = url)
    }
  }

  private getFile(): FileUploaded {

    if (this.material === null) {
      return null;
    }

    const file = this.material?.files[0];

    if (!file) {
      return null;
    }

    return file;
  }

  delete() {
    this.store.dispatch(MaterialActions.delete.request({
      input: {
        courseId: this.courseId,
        materialId: this.material.id
      }
    }))
  }

  openLocalFile(event: Event) {
    let fileId = getFileId(this.material.files[0]);

    if ([
      FileState.Downloaded,
      FileState.NeedsToBeUploaded,
      FileState.IsUploading
    ].includes(this.getFile().status.currently)) {
      event.preventDefault();
    } else {
      return;
    }



    this.fileApi.getFile<Blob>(fileId).subscribe(
      file => {
        console.log({ fileId, file })
        const url = URL.createObjectURL(file)
        var a = document.createElement("a");
        a.href = url;
        // a.target = 'texto';
        // Don't set download attribute
        a.download = this.material.files[0].fileName;
        a.click();

        // window.open(url, '_blank');

        setTimeout(() => {
          URL.revokeObjectURL(url)
        }, 500);
      }
    )
  }

}
