import { Component, Input } from '@angular/core';
import { FileState } from 'src/app/models/file-uploaded.model';

@Component({
  selector: 'app-sync-status-icon',
  templateUrl: './sync-status-icon.component.html',
  styleUrls: ['./sync-status-icon.component.css']
})
export class SyncStatusIconComponent {
  @Input() syncStatus: FileState = FileState.IsDownloading;

  constructor() { }

}
