import { Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { SharedService } from './../shared.service';

@Component({
  selector: 'app-confirm-delete',
  templateUrl: './confirm-delete.component.html',
  styleUrls: ['./confirm-delete.component.css']
})
export class ConfirmDeleteComponent implements OnInit {

  @ViewChild('content', { static: true }) content: ElementRef;
  @Input() title: string;
  @Output() ok: EventEmitter<boolean> = new EventEmitter<boolean>();

  /**
   * Reference to the modal that enables closing it with .ts commands
   */
  modalRef: NgbActiveModal;

  translationText = SharedService.translationText.ConfirmDeleteComponent;

  constructor(
    private modalService: NgbModal,
    private sharedService: SharedService
  ) { }


  ngOnInit() {
    if (!this.title) {
      this.title = this.translationText?.title;
    }
  }

  open() {
    this.modalRef = this.modalService.open(this.content);
    console.log('Abrindo modal');
  }
}
