import { Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { NgbActiveModal, NgbModal, NgbModalOptions } from '@ng-bootstrap/ng-bootstrap';
import { SharedService } from './../shared.service';

@Component({
  selector: 'app-modal-form',
  templateUrl: './modal-form.component.html',
  styleUrls: ['./modal-form.component.css']
})
export class ModalFormComponent implements OnInit {

  @ViewChild('content', { static: true }) content: ElementRef;
  @Input() title: string;
  @Input() screenSize: 'sm' | 'md' | 'lg' | 'xl';
  @Input() centered = false;
  @Output() ok: EventEmitter<boolean> = new EventEmitter<boolean>();

  modalRef: NgbActiveModal;
  translationText = SharedService.translationText.ModalFormComponent;
  closed = false;

  constructor(
    private modalService: NgbModal,
    private sharedService: SharedService
  ) { }


  ngOnInit() {
    if (this.title === null) {
      this.title = this.translationText?.title;
    }
  }

  /**
   * Open an NgBootstrap modal
   *
   */
  open() {
    const options: NgbModalOptions = {};
    options.centered = this.centered;

    if (this.screenSize) {
      options.size = this.screenSize;
    }

    this.modalRef = this.modalService.open(this.content, options);

    this.closed = false;
    console.log('Opening modal: ' + this.title);
  }

  dismiss() {
    this.closed = true;
    this.modalRef.dismiss('Cross click');
    console.log('Dismissing modal: ' + this.title);
    this.ok.emit(false);
  }

  close(ok?: boolean) {
    this.closed = true;
    this.modalRef.close();
    console.log('Closing modal: ' + this.title);

    if (ok !== null) {
      this.ok.emit(ok);
    }
  }
}
