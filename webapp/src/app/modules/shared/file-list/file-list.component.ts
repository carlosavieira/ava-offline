import { Component, EventEmitter, Input, Output, TrackByFunction } from '@angular/core';
import { FileUploaded } from 'src/app/models/file-uploaded.model';
import { getFileId } from 'src/app/state/file-uploaded/file-uploaded.state';
import { SharedService } from './../shared.service';


@Component({
  selector: 'app-file-list',
  templateUrl: './file-list.component.html',
  styleUrls: ['./file-list.component.css']
})
export class FileListComponent {

  @Input() files: (File | FileUploaded)[];
  @Output() filesChanged: EventEmitter<(File | FileUploaded)[]> = new EventEmitter();
  @Input() editable = false;
  @Input() modified = false;
  @Output() check: EventEmitter<void> = new EventEmitter<void>();
  @Output() undo: EventEmitter<void> = new EventEmitter<void>();

  translationText: any = {};

  constructor(private sharedService: SharedService) {
    this.restoreFiles();
  }

  trackFile: TrackByFunction<File | FileUploaded> = (index: number, file) => {
    if (file instanceof File) {
      return file.name;
    } else if (!!file) {
      return getFileId(file);
    }

    return null;
  }

  removeFile(id: number) {
    if (this.editable) {
      // Remove the specified item
      this.files.splice(id, 1);

      // Ask the parent component to check the file list, because it was updated
      this.check.emit();
    }
  }

  removeAll() {
    if (this.editable) {

      // Remove all items
      this.files.splice(0, this.files.length);

      // Ask the parent component to check the file list, because it was updated
      this.check.emit();
    }
  }

  restoreFiles() {
    this.undo.emit();
  }

}
