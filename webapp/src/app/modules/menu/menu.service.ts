import { Injectable } from '@angular/core';
import { IconName, IconPrefix } from '@fortawesome/fontawesome-common-types';
import { Store } from '@ngrx/store';
import deepEqual from 'deep-equal';
import { combineLatest, Observable } from 'rxjs';
import { distinctUntilChanged, map, startWith } from 'rxjs/operators';
import { MenuItem } from 'src/app/models/menu';
import { UserSM } from 'src/app/models/user.model';
import { selectRouteParam } from 'src/app/state/router/router.selector';
import { UserSelectors } from 'src/app/state/user/user.selector';
import { SharedService } from '../shared/shared.service';

const HOME_ITEMS: MenuItem[] = [{
  icon_type: 'fas',
  icon: 'book-open',
  link: `/courses`,
  text: 'Cursos'
},
{
  icon_type: 'far',
  icon: 'calendar-alt',
  link: `/calendar`,
  text: 'Calendário'
},
{
  icon_type: 'far',
  icon: 'chart-bar',
  link: `/grades`,
  text: 'Desempenho'
}];

const ADMIN_ITEMS: MenuItem[] = [{
  icon_type: 'fas',
  icon: 'book-open',
  link: `/admin/courses`,
  text: 'Cursos'
},
{
  icon_type: 'fas',
  icon: 'user-shield',
  link: `/admin/list`,
  text: 'Administradores'
}]


@Injectable()
export class MenuService {

  constructor(private sharedService: SharedService, private store: Store) { }

  getItems(): Observable<MenuItem[]> {
    const info$: Observable<[string, UserSM]> = combineLatest([
      this.store.select(selectRouteParam('courseId')),
      this.store.select(UserSelectors.current)
    ])

    return info$.pipe(
      distinctUntilChanged<[string, UserSM]>(deepEqual),
      map(([courseId, user]) => {
        if (courseId) {
          return this.getCourse(Number(courseId));
        } else if (user?.isAdmin) {
          return ADMIN_ITEMS;
        }

        return HOME_ITEMS;
      }),
      startWith([]),
    )
  }

  private getCourse(courseId: number): MenuItem[] {
    return [
      {
        icon_type: 'far',
        icon: 'newspaper',
        link: `/courses/${courseId}/wall`,
        text: 'Mural'
      },
      {
        icon_type: 'fas',
        icon: 'box-open',
        link: `/courses/${courseId}/material`,
        text: 'Material de Apoio'
      },
      {
        icon_type: 'fas',
        icon: 'tasks',
        link: `/courses/${courseId}/activities`,
        text: 'Atividades'
      },
      {
        icon_type: 'custom' as IconPrefix,
        icon: 'grades' as IconName,
        link: `/courses/${courseId}/grades`,
        text: 'Notas'
      },
      {
        icon_type: 'fas',
        icon: 'user-friends',
        link: `/courses/${courseId}/members`,
        text: 'Participantes'
      },
      {
        icon_type: 'far',
        icon: 'calendar-alt',
        link: `/courses/${courseId}/calendar`,
        text: 'Calendário'
      },
    ];
  }


  getProfilePhoto(userId: number) {
    const url = this.sharedService.downloadLink(
      'users/' + userId + '/picture'
    )

    return url;
  }
}
