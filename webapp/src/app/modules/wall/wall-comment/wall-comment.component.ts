import { Component, ElementRef, Input, OnInit, ViewChild } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { take } from 'rxjs/operators';
import { WallComment } from 'src/app/models/wall-comment.model';
import { WallPost } from 'src/app/models/wall-post.model';
import { CourseSelectors } from 'src/app/state/course/course.selector';
import { ParticipationAdvancedSelectors } from 'src/app/state/participation/participation.advanced.selector';
import { UserAdvancedSelectors } from 'src/app/state/user/user.advanced.selector';
import { WallCommentActions } from 'src/app/state/wall-comment/wall-comment.actions';
import { WallService } from './../wall.service';

@Component({
  selector: 'app-wall-comment',
  templateUrl: './wall-comment.component.html',
  styleUrls: ['./wall-comment.component.css']
})
export class WallCommentComponent implements OnInit {

  @Input() comment: WallComment;
  @Input() post: WallPost;
  @ViewChild('commentElement', { static: true }) commentRef: ElementRef;

  canDelete$: Observable<boolean>;
  expanded = true;

  courseId: number;
  profilePictureLink: Observable<string>;
  commentText: string;

  translationText = WallService.translationText.WallCommentComponent;

  constructor(
    private store: Store
  ) { }

  ngOnInit() {
    setTimeout(() => {
      this.expand(this.commentRef);
    }, 100);

    this.canDelete$ = this.store.select(ParticipationAdvancedSelectors.hasPermission('delete_post_comment'));
    this.store.select(CourseSelectors.currentId).pipe(take(1)).subscribe(id => this.courseId = Number(id));

    this.commentText = this.comment.text?.trim();

    this.profilePictureLink = this.store.select(UserAdvancedSelectors.pictureLink.userId, { userId: this.comment.createdBy?.id });
  }

  likeToggle() {
    this.store.dispatch(WallCommentActions.like.request({
      input: {
        to: !this.comment.liked,
        commentId: this.comment.id,
        courseId: this.courseId,
        postId: this.post.id
      }
    }));
  }

  deleteComment(confirmed: number) {
    if (!confirmed) {
      return;
    }

    this.store.dispatch(WallCommentActions.delete.request({
      input: {
        commentId: this.comment.id,
        courseId: this.courseId,
        postId: this.post.id
      }
    }))
  }

  expand(e: ElementRef) {
    this.expanded = e.nativeElement.scrollHeight < 45;
  }
}
