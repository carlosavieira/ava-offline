import { Component, Input, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { take } from 'rxjs/operators';
import { WallComment } from 'src/app/models/wall-comment.model';
import { WallPost } from 'src/app/models/wall-post.model';
import { CourseSelectors } from 'src/app/state/course/course.selector';
import { WallCommentActions } from 'src/app/state/wall-comment/wall-comment.actions';
import { WallCommentAdvancedSelectors } from 'src/app/state/wall-comment/wall-comment.advanced.selector';
import { WallCommentSelectors } from 'src/app/state/wall-comment/wall-comment.selector';
import { WallService } from './../wall.service';

@Component({
  selector: 'app-wall-item',
  templateUrl: './wall-item.component.html',
  styleUrls: ['./wall-item.component.css']
})
export class WallItemComponent implements OnInit {
  @Input() post: WallPost;

  courseId: number;

  comments$: Observable<WallComment[]>;
  translationText = WallService.translationText.WallItemComponent;

  constructor(private store: Store) { }

  isActivity() {
    return this.post?.activityId > 0;
  }

  ngOnInit() {
    this.store.select(CourseSelectors.currentId).pipe(take(1)).subscribe(id => this.courseId = Number(id))

    // Only request if not created offline
    if (this.post.id < 10000000000) {
      this.store.dispatch(WallCommentActions.fetchAll.request({ input: { postId: this.post.id, courseId: this.courseId } }))
    }

    this.comments$ = this.store.select(WallCommentAdvancedSelectors.sel.many(WallCommentSelectors.byPost.id.all(this.post.id))).pipe(
    )
  }

  private sortComments(a: WallComment, b: WallComment) {
    const dateA = a?.lastModifiedDate;
    const dateB = b?.lastModifiedDate;

    return dateA > dateB ? 1 : (dateA < dateB ? -1 : 0);
  }


  trackComment(index: number, comment: WallComment) {
    return comment?.id;
  }
}
