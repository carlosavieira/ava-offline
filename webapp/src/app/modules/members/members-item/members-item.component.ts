import { Component, Input, OnInit } from '@angular/core';
import { Role } from 'src/app/models/role.model';
import { User } from 'src/app/models/user.model';
import { SharedService } from '../../shared/shared.service';
import { MembersService } from '../members.service';

@Component({
  selector: 'app-members-item',
  templateUrl: './members-item.component.html',
  styleUrls: ['./members-item.component.css']
})
export class MembersItemComponent implements OnInit {
  @Input() user: User;
  @Input() role: Role;

  roleName: string;
  canGetEvaluation: boolean;
  photoUrl: string;
  showCap: boolean;

  constructor(
    private sharedService: SharedService,
    private membersService: MembersService) {
  }

  ngOnInit() {
    this.photoUrl = this.membersService.getProfilePhoto(this.user?.id);
    this.canGetEvaluation = this.sharedService.hasPermission('get_all_evaluations');
    this.showCap = this.role.permissions.some(p => p.name === 'edit_course');
    this.roleName = this.role.name === 'TEACHER'
      ? 'Professor'
      : this.role.name === 'STUDENT'
        ? 'Aluno'
        : this.role.name
  }
}
