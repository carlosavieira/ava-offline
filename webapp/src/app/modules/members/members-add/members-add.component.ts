import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { MembersService } from '../members.service';

@Component({
  selector: 'app-members-add',
  templateUrl: './members-add.component.html',
  styleUrls: ['./members-add.component.css']
})
export class MembersAddComponent implements OnInit {

  key: string;
  @Input() courseId: number;
  @Output() closeForm = new EventEmitter<void>();

  constructor(private membersService: MembersService) {
  }

  ngOnInit() {
    this.membersService.getCourseKey(this.courseId).subscribe((data: { key: string }) => {
      this.key = data.key;
    });
  }

  refreshKey() {
    this.membersService.changeCourseKey(this.courseId).subscribe((data: { key: string }) => {
      this.key = data.key;
    });
  }

  copyKey(key: string) {
    document.addEventListener('copy', (e: ClipboardEvent) => {
      e.clipboardData.setData('text/plain', (key));
      e.preventDefault();
      document.removeEventListener('copy', null);
    });
    document.execCommand('copy');
  }
}
