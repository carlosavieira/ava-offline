import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import deepEqual from 'deep-equal';
import { BehaviorSubject, combineLatest, Observable, of } from 'rxjs';
import { distinctUntilChanged, map, switchMap } from 'rxjs/operators';
import { RoleSM } from 'src/app/models/role.model';
import { User } from 'src/app/models/user.model';
import { CourseSelectors } from 'src/app/state/course/course.selector';
import { ParticipationActions } from 'src/app/state/participation/participation.actions';
import { ParticipationAdvancedSelectors } from 'src/app/state/participation/participation.advanced.selector';

@Component({
  selector: 'app-members',
  templateUrl: './members.component.html',
  styleUrls: ['./members.component.css']
})
export class MembersComponent implements OnInit {
  courseId: number;
  canAddMember$: Observable<boolean>;


  users$: Observable<User[][]>
  activeRoles$: Observable<RoleSM[]>
  roles$: Observable<RoleSM[]>
  isAdmin$: Observable<boolean>

  filter$: BehaviorSubject<RoleSM>
  sortBy$: BehaviorSubject<string>
  reverseSort$: BehaviorSubject<boolean>

  sortText = { name: 'Nome'/* , creationDate: 'Data de criação' */ };
  sortOptions = Object.keys(this.sortText);

  setObservables() {
    this.canAddMember$ = this.store.select(ParticipationAdvancedSelectors.hasPermission('edit_course'));

    // UI Controls
    this.filter$ = new BehaviorSubject(null);
    this.sortBy$ = new BehaviorSubject('name');
    this.reverseSort$ = new BehaviorSubject(false);

    // Users and respective roles in the course
    const allUsersAndRoles$ = this.store.select(ParticipationAdvancedSelectors.ofCurrentCourse.usersAndRoles).pipe(
      distinctUntilChanged<{ user: User, role: RoleSM }[]>(deepEqual),
    )

    // All roles active in this course. To be used in filter selection
    this.activeRoles$ = this.store.select(ParticipationAdvancedSelectors.ofCurrentCourse.uniqueRoles).pipe(
      distinctUntilChanged<RoleSM[]>(deepEqual),
    );

    // Roles to display
    this.roles$ = this.filter$.pipe(
      switchMap((role) => !role
        ? this.activeRoles$
        : of([role])
      ),
      distinctUntilChanged<RoleSM[]>(deepEqual),
    );

    // Group users by role
    const usersGroupedByRole$ = combineLatest([this.roles$, allUsersAndRoles$]).pipe(
      distinctUntilChanged<[RoleSM[], { user: User, role: RoleSM }[]]>(deepEqual),
      map(([roles, usersAndRoles]) => roles.map(role =>
        usersAndRoles.filter(ur => ur.role.id === role.id)
          ?.map(ur => ur?.user)
      )),
    )


    // Sort user inside each role
    this.users$ = combineLatest([usersGroupedByRole$, this.sortBy$, this.reverseSort$]).pipe(
      map(([userGroups, sortBy, reverse]) => userGroups.map(
        users => {
          const rev = reverse ? -1 : 1;

          // TODO: Implement other sorting methods besides using the user name
          return users.sort((a, b) => a.name.localeCompare(b.name) * rev)
        }
      )),
      distinctUntilChanged<User[][]>(deepEqual),
    )
  }

  constructor(
    private store: Store
  ) {
  }

  ngOnInit() {

    // Get current course id
    this.store.select(CourseSelectors.currentId).pipe(
      distinctUntilChanged()
    ).subscribe({ next: (id: number) => this.courseId = id });

    this.setObservables();

    this.store.dispatch(ParticipationActions.fetchAllUsersWithRoles.request({ input: { courseId: this.courseId } }));
  }

  reverse() {
    this.reverseSort$.next(!this.reverseSort$.getValue());
  }

  filter(role: RoleSM) {
    this.filter$.next(role);
  }

  sort(value: string) {
    this.sortBy$.next(value);
  }

  getFilterText(r: RoleSM) {
    return !r
      ? 'Todos'
      : r.name === 'TEACHER'
        ? 'Professores'
        : r.name === 'STUDENT'
          ? 'Alunos'
          : r.name
  }
}
