import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { LoginData } from 'src/app/models/login-data';
import { RegisterFinish } from 'src/app/models/register';
import { LoginService } from './../login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginData: LoginData;
  loginForm: FormGroup;
  registerFinish: RegisterFinish;
  returnUrl: string;

  errorMessage = '';
  registerOK = false;
  translationText = LoginService.translationText.LoginComponent;

  emailIsFocused = false;
  show = false;

  /**
   * Store the error messages to be shown for each form input.
   *
   */
  formErrors = {
    userEmail: '',
    password: ''
  };

  constructor(
    private fb: FormBuilder,
    private loginService: LoginService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit(): void {

    // Get return url from route parameters
    this.route.queryParams.subscribe(params => {

      this.returnUrl = params.returnUrl || '';

      this.registerFinish = {
        email: params['email'] || '',
        hash: params['hash'] || ''
      }
    });

    this.newLoginData();

    if (this.registerFinish.email !== '' && this.registerFinish.hash !== '') {
      this.loginService.registerFinish(this.registerFinish).subscribe(data => {
        this.registerOK = true;
        console.log('finishing registration...')
      });
    }
  }

  showPassword() {
    this.show = !this.show;
  }

  onSubmit() {
    this.loginData = {
      userEmail: this.loginForm.controls['userEmail'].value,
      password: this.loginForm.controls['password'].value
    }

    // Send login data
    this.loginService.login(this.loginData).subscribe({
      next: (data) => {
        this.router.navigate([this.returnUrl]);
      },
      error: () => {
        this.errorMessage = this.translationText.loginError;
      }
    });
  }

  newLoginData() {
    this.loginData = {} as LoginData;

    this.buildForm();
  }

  buildForm(): void {

    this.loginForm = this.fb.group({
      userEmail: [
        { value: this.registerFinish.email, disabled: this.registerFinish.email !== '' },
        [Validators.required, Validators.minLength(3), Validators.email]],
      password: ['', [Validators.required, Validators.minLength(8)]]
    });
  }
}
