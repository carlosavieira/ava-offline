import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { LoginService } from '../login.service';

@Component({
  selector: 'app-login-forgot-password',
  templateUrl: './login-forgot-password.component.html',
  styleUrls: ['./login-forgot-password.component.css']
})
export class LoginForgotPasswordComponent implements OnInit {

  forgotPasswordForm: FormGroup;
  translationText = LoginService.translationText.LoginForgotPasswordComponent;

  errorMessage = '';
  successMessage = '';

  constructor(
    private fb: FormBuilder,
    private loginService: LoginService,
    private router: Router
  ) { }

  ngOnInit() {
    this.buildForm();
  }

  buildForm(): void {
    const control = new FormControl('bad@', Validators.email);

    this.forgotPasswordForm = this.fb.group({
      email: ['', [
        Validators.required,
        Validators.email
      ]],
    });
  }

  onSubmit() {
    this.loginService.forgotPassword(this.forgotPasswordForm.value).subscribe(data => {
      if (data) {
        this.errorMessage = this.translationText.errorMessage;
        this.successMessage = '';
      } else {
        this.errorMessage = '';
        this.successMessage = this.translationText.successMessage;

        // this.router.navigate(['/courses']);
      }
    });
  }
}