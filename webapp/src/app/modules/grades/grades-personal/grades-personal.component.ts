import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { ActivityItem } from 'src/app/models/activity-item.model';
import { Activity, ActivityStates } from 'src/app/models/activity.model';
import { Course } from 'src/app/models/course.model';
import { FinalGrade } from 'src/app/models/final-grade';
import { User } from 'src/app/models/user.model';
import { ActivityItemAdvancedSelectors } from 'src/app/state/activity-item/activity-item.advanced.selector';
import { ActivityAdvancedSelectors } from 'src/app/state/activity/activity.advanced.selector';
import { ActivitySelectors } from 'src/app/state/activity/activity.selector';
import { GradesFinalActions } from 'src/app/state/grades-final/grades-final.actions';
import { GradesFinalSelectors } from 'src/app/state/grades-final/grades-final.selector';
import { GradesInfoSelectors } from 'src/app/state/grades-info/grades-info.selector';
import { ActivitiesService } from '../../activities/activities.service';
import { GradesService } from '../grades.service';
import { GradesComponent } from '../grades/grades.component';

@Component({
  selector: 'app-grades-personal',
  templateUrl: './grades-personal.component.html',
  styleUrls: ['./grades-personal.component.css']
})


export class GradesPersonalComponent implements OnInit {
  translationText = GradesService.translationText.GradesPersonalComponent;

  courseId: number;
  userId: number;

  activities$: Observable<Activity[]>
  items$: Observable<ActivityItem[]>
  finalGrade$: Observable<FinalGrade>

  constructor(private store: Store, private activitiesService: ActivitiesService, private route: ActivatedRoute, private gradesComponent: GradesComponent) {
    this.route.data.subscribe((data: { course: Course, user: User }) => {
      this.courseId = data.course?.id;
      this.userId = data.user?.id;
    })
  }

  ngOnInit(): void {
    this.store.dispatch(GradesFinalActions.getUserOverview.request({
      input: {
        courseId: this.courseId,
        userId: this.userId
      }
    }));

    this.activities$ = this.store.select(
      ActivityAdvancedSelectors.sel.many(
        ActivitySelectors.byCourse.id.all(this.courseId)
      )
    );

    this.items$ = this.store.select(
      ActivityItemAdvancedSelectors.sel.many(
        ActivityItemAdvancedSelectors.byCourseId(this.courseId)
      )
    );

    this.finalGrade$ = this.store.select(
      GradesFinalSelectors.byCourseIdAndUserId({
        courseId: this.courseId,
        userId: this.userId
      })
    );
  }

  getItem$(activity: Activity) {
    return this.items$.pipe(
      map(items => items.find(item => item?.activity?.id === activity?.id)),
      map(item => ({
        item,
        evaluated: !!item?.evaluation?.id,
        submitted: !!item?.submission?.id
      }))
    );
  }

  getAverage$(activity: Activity) {
    return this.store.select(GradesInfoSelectors.byId(activity?.id));
  }

  getStatusText(activity: Activity, item?: ActivityItem) {
    const status = this.activitiesService.getActivityState(activity);

    switch (status) {
      case ActivityStates.GradesReleased:
        return item?.evaluation?.score;
      case ActivityStates.SubmissionEnded:
        return item?.submission?.id
          ? this.translationText.submitted
          : this.translationText.notSubmitted;
      case ActivityStates.SubmissionStarted:
        return item?.submission?.id
          ? this.translationText.submitting
          : this.translationText.notSubmitting;
      default:
        return '-'
    }
  }
}
