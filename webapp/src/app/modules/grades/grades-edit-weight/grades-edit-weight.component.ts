import { Component, EventEmitter, Injector, Output } from '@angular/core';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { Actions, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { filter, map, take, tap } from 'rxjs/operators';
import { GradesConfig, GradesConfigForm } from 'src/app/models/grades-config';
import { GradesConfigActions } from 'src/app/state/grades-config/grades-config.actions';
import { GradesConfigAdvancedSelectors } from 'src/app/state/grades-config/grades-config.advanced.selector';
import { GradesConfigSelectors } from 'src/app/state/grades-config/grades-config.selector';
import { Form } from 'src/app/templates/form';
import { GradesService } from '../grades.service';

@Component({
  selector: 'app-grades-edit-weight',
  templateUrl: './grades-edit-weight.component.html',
  styleUrls: ['./grades-edit-weight.component.css']
})
export class GradesEditWeightComponent extends Form<GradesConfigForm> {

  @Output() sent: EventEmitter<void> = new EventEmitter();

  translationText = GradesService.translationText.GradesEditWeightComponent;
  lastConfig: GradesConfig;
  editWeights: boolean;
  weightSum: number;


  weightArray: FormGroup[];

  constructor(
    private injector: Injector,
    private store: Store,
    private actions$: Actions,
  ) {
    super(injector);
  }

  buildForm() {
    const form = new FormGroup({
      useArithmeticMean: new FormControl(this.lastConfig?.useArithmeticMean),
      publishOnWall: new FormControl(false),
      defaultWeight: new FormControl(this.value?.defaultWeight),
      weights: new FormArray(
        (!(this.lastConfig?.gradedActivities)) ? [] : this.lastConfig.gradedActivities.map(({ id, gradeWeight }) => new FormGroup({
          activityId: new FormControl(id, Validators.required),
          weight: new FormControl(gradeWeight, [Validators.required, Validators.min(0.01)])
        }))
      )
    });

    this.editWeights = !(this.lastConfig?.useArithmeticMean);
    this.setWeightArray(form);

    return form;
  }

  onValueChange2() {
    this.editWeights = !(this.form.value?.useArithmeticMean);

    this.weightSum = (this.form.value as GradesConfigForm)?.weights
      ?.map(item => item.weight).reduce((a, b) => a + b, 0);
  }

  getBlank() {
    return {} as GradesConfigForm;
  }


  valuePointer(): Observable<GradesConfigForm> {
    return this.store.select(GradesConfigAdvancedSelectors.sel.one(
      GradesConfigSelectors.byCourseId(this.courseId),
    )).pipe(
      filter(config => !!config?.id),
      take(1),
      tap((config: GradesConfig) => {
        this.lastConfig = config;
        this.editWeights = !config.useArithmeticMean;
      })
    )
  }



  create() {
    console.error('The grade weights are not created manually by the user, only edited');
  }

  edit() {
    if (this.editWeights) {
      this.store.dispatch(GradesConfigActions.editWeights.request({
        input: {
          courseId: this.courseId,
          form: this.form.value as GradesConfigForm
        }
      }))

      this.actions$.pipe(
        ofType(GradesConfigActions.editWeights.success),
        map(({ data }) => {
          console.log(data);
          // this.lastConfig = config;
          this.sent.emit();
        })
      ).subscribe();
    } else if (!this.lastConfig.useArithmeticMean) {
      this.store.dispatch(GradesConfigActions.useArithmeticMean.request({
        input: {
          courseId: this.courseId,
        }
      }))

      this.actions$.pipe(
        ofType(GradesConfigActions.useArithmeticMean.success),
        map(({ data }) => {
          console.log(data);
          // this.lastConfig = config;
          this.sent.emit();
        })
      ).subscribe();

    } else {
      console.warn('No changes were made =D')
      this.sent.emit();
    }
  }

  setWeightArray(form: FormGroup) {
    this.weightArray = (form.controls['weights'] as FormArray).controls as FormGroup[];
  }
}
