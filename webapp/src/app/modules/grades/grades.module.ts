import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { GradesRoutingModule } from './grades-routing.module';
import { GradesComponent } from './grades/grades.component';
import { GradesService } from './grades.service';
import { GradesPersonalComponent } from './grades-personal/grades-personal.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { GradesCourseComponent } from './grades-course/grades-course.component';
import { SharedModule } from '../shared/shared.module';
import {ReactiveFormsModule,FormsModule} from '@angular/forms';
import { GradesEditWeightComponent } from './grades-edit-weight/grades-edit-weight.component'

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    NgbModule,
    RouterModule,
    GradesRoutingModule,
    FontAwesomeModule,
    SharedModule,
    ReactiveFormsModule,
    FormsModule,
  ],
  declarations: [
    GradesComponent,
    GradesPersonalComponent,
    GradesCourseComponent,
    GradesEditWeightComponent,
  ],
  providers:[
    GradesService
  ],
  exports: [
    GradesComponent,
  ]

})
export class GradesModule { }
