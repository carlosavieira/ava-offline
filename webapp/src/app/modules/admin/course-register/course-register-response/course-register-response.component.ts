import { Component, Input, OnInit } from '@angular/core';
import { CourseRegisterService } from './../course-register.service';


@Component({
  selector: 'app-course-register-response',
  templateUrl: './course-register-response.component.html',
  styleUrls: ['./course-register-response.component.css']
})
export class CourseRegisterResponseComponent implements OnInit {

  @Input() response: any;
  @Input() submitted = false;
  error: boolean;
  translationText = CourseRegisterService.translationText.CourseRegisterResponseComponent;

  constructor(private courseRegisterService: CourseRegisterService) {
    this.error = true;
  }

  ngOnInit() {
    if (this.response && this.response.courseId) {
      this.error = false;
    }

  }

}
