import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { LANGUAGE } from 'src/app/dev/languages';
import { Course, CourseForm, CourseJson, fromJsonToCourse } from 'src/app/models/course.model';


@Injectable()
export class CourseRegisterService {

    static translationText = LANGUAGE.courseRegister;

    constructor(private http: HttpClient) { }

    addCourse(course: CourseForm): Observable<Course> {
        const url = 'courses';

        return this.http.post<CourseJson>(url, course).pipe(
            map(data => fromJsonToCourse(data))
        );
    }
}
