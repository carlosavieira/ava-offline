import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Course } from 'src/app/models/course.model';
import { CourseRegisterService } from './../course-register.service';

@Component({
  selector: 'app-course-register-form',
  providers: [CourseRegisterService],
  templateUrl: './course-register-form.component.html',
  styleUrls: ['./course-register-form.component.css']
})
export class CourseRegisterFormComponent implements OnInit {

  @Output() response: EventEmitter<any> = new EventEmitter<any>();

  translationText = CourseRegisterService.translationText.CourseRegisterFormComponent;
  submitted = false;
  course: Course;
  courseForm: FormGroup;
  memberList: number[];
  userRoles: number[];

  active = true;

  formErrors = {
    name: '',
    info: '',
    key: '',
    professor: '',
    subscriptionBegin: '',
    subscriptionEnd: '',
    startDate: '',
    endDate: '',
    noMaxStudents: 0
  };


  constructor(private fb: FormBuilder, private courseRegisterService: CourseRegisterService, private router: Router) { }

  onSubmit() {
    this.submitted = true;
    this.course = this.courseForm.value;
    this.addCourse(this.course);
  }

  addCourse(course: Course) {
    this.courseRegisterService.addCourse(course).subscribe((res) => {
      this.response.emit(res);
      console.log(res);
      this.router.navigate(['admin/courses']);
    });
  }

  newCourse() {
    this.course = {} as Course;
    this.buildForm();

    this.active = false;
    setTimeout(() => this.active = true, 0);
  }

  /**
   * On initialization, instantiate a new Course object;
   */
  ngOnInit(): void {
    this.newCourse();

  }

  buildForm(): void {
    this.courseForm = this.fb.group({
      name: [this.course?.name, [Validators.required, Validators.minLength(3)]],
      info: [this.course?.info, [Validators.required]],
      subscriptionBegin: [this.course?.subscriptionBegin, [Validators.required]],
      subscriptionEnd: [this.course?.subscriptionEnd, [Validators.required]],
      startDate: [this.course?.startDate, [Validators.required]],
      endDate: [this.course?.endDate, [Validators.required]],
      noMaxStudents: [this.course?.noMaxStudents, [Validators.required]]
    });

    this.courseForm.valueChanges
      .subscribe(data => this.onValueChanged(data));

    this.onValueChanged();
  }

  onValueChanged(data?: any) {
    if (!this.courseForm) { return; }
    const form = this.courseForm;

    for (const field in this.formErrors) {
      // clear previous error message (if any)
      this.formErrors[field] = '';
      const control = form.get(field);

      if (control && control.dirty && !control.valid) {
        const messages = this.translationText?.validationMessages[field];
        // eslint-disable-next-line guard-for-in
        for (const key in control.errors) {
          this.formErrors[field] += messages[key] + ' ';
        }
      }
    }
  }
}
