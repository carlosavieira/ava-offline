import { Component, HostListener, Input, OnInit } from '@angular/core';
import { User } from 'src/app/models/user.model';
import { AdminService } from './../admin.service';

@Component({
  selector: 'app-admin-list',
  templateUrl: './admin-list.component.html',
  styleUrls: ['./admin-list.component.css']
})
export class AdminListComponent implements OnInit {

  @Input() activityId: number;

  admins: User[];

  constructor(private adminService: AdminService) { }

  screenSize: 'small' | 'normal' | 'big' = 'big';


  translationText = AdminService.translationText.AdminListComponent;

  ngOnInit() {
    this.getAdmins();
  }

  getAdmins() {
    this.adminService.getAdmins().subscribe((res) => {
      this.admins = res;
    });
  }

  @HostListener('window:resize', ['$event'])
  updateScreenSize(event) {
    const size = window.screen.availWidth;

    if (size > 500) {
      this.screenSize = 'big';
    } else {
      this.screenSize = 'small';
    }
  }
}
