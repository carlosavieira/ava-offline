import { Component, HostListener, Input, OnInit } from '@angular/core';
import { Course } from 'src/app/models/course.model';
import { AdminService } from './../admin.service';

@Component({
  selector: 'app-admin-list-courses',
  templateUrl: './admin-list-courses.component.html',
  styleUrls: ['./admin-list-courses.component.css']
})
export class AdminListCoursesComponent implements OnInit {

  /**
   * Activity identification
   *
   */
  @Input() activityId: number;
  courses: Course[];

  constructor(
    private adminService: AdminService
  ) { }


  screenSize: 'small' | 'normal' | 'big' = 'big';

  translationText = AdminService.translationText.AdminListCoursesComponent;

  ngOnInit() {
    this.getCourses();
  }

  getCourses() {
    this.adminService.getCourses().subscribe((res) => {
      this.courses = res;
    });
  }


  @HostListener('window:resize', ['$event'])
  updateScreenSize(event) {
    const size = window.screen.availWidth;

    if (size > 500) {
      this.screenSize = 'big';
    } else {
      this.screenSize = 'small';
    }
  }
}
