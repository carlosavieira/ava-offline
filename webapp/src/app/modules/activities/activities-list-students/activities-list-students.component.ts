import { Component, HostListener, Input, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { combineLatest, Observable } from 'rxjs';
import { map, withLatestFrom } from 'rxjs/operators';
import { ActivityItem } from 'src/app/models/activity-item.model';
import { Activity, ActivityStates } from 'src/app/models/activity.model';
import { User } from 'src/app/models/user.model';
import { ActivitiesApiService } from 'src/app/services/api/activities.api.service';
import { ActivityItemActions } from 'src/app/state/activity-item/activity-item.actions';
import { ActivityItemAdvancedSelectors } from 'src/app/state/activity-item/activity-item.advanced.selector';
import { ActivityItemSelectors } from 'src/app/state/activity-item/activity-item.selector';
import { ParticipationActions } from 'src/app/state/participation/participation.actions';
import { ParticipationAdvancedSelectors } from 'src/app/state/participation/participation.advanced.selector';
import { ActivitiesService } from '../activities.service';

@Component({
  selector: 'app-activities-list-students',
  templateUrl: './activities-list-students.component.html',
  styleUrls: ['./activities-list-students.component.css']
})
export class ActivitiesListStudentsComponent implements OnInit {
  translationText = ActivitiesService.translationText.ActivitiesListStudentsComponent;

  @Input() activityStatus: ActivityStates;
  @Input() activity: Activity;
  @Input() courseId: number;

  allItems$: Observable<ActivityItem[]>;
  allStudents$: Observable<User[]>;
  usersWithoutSubmission$: Observable<User[]>;

  evaluatedItems$: Observable<ActivityItem[]>;
  submittedButNotEvaluated$: Observable<ActivityItem[]>;
  itemsToEvaluate$: Observable<ActivityItem[]>;

  screenSize: 'small' | 'normal' | 'big' = 'big';
  collapseList = false;

  public get states(): typeof ActivityStates {
    return ActivityStates;
  }

  constructor(private store: Store, private activitiesApi: ActivitiesApiService) { }

  ngOnInit(): void {
    this.setObservables();
  }


  setObservables() {
    this.store.dispatch(ActivityItemActions.fetchAll.request({
      input: {
        activityId: this.activity.id,
        courseId: this.courseId
      }
    }));

    this.allItems$ = this.store.select(ActivityItemAdvancedSelectors.sel.many(
      ActivityItemSelectors.byActivityId(this.activity.id)
    ))

    this.store.dispatch(ParticipationActions.fetchAllUsersWithRoles.request({
      input: {
        courseId: this.courseId
      }
    }))

    const studentRoleIds$ = this.store.select(ParticipationAdvancedSelectors.ofCurrentCourse.uniqueRoles).pipe(
      map(roles => roles?.filter(role =>
        role.permissions.some(permission => permission.name == 'create_activity_submissions') ?? []
      )),
      map(roles => roles.map(role => role.id))
    )

    this.allStudents$ = this.store.select(
      ParticipationAdvancedSelectors.ofCurrentCourse.usersAndRoles
    ).pipe(
      withLatestFrom(studentRoleIds$),
      map(([usersAndRoles, roleIds]) => usersAndRoles
        .filter(item => roleIds.includes(item.role.id))
        .map(item => item.user)
      )
    )

    this.evaluatedItems$ = this.allItems$.pipe(
      map(items => items?.filter(item => item?.evaluation?.id) ?? [])
    )

    this.usersWithoutSubmission$ = combineLatest([this.allStudents$, this.allItems$]).pipe(
      map(([users, items]) => users?.filter(user => !items.some(item => item.user?.id === user.id)) ?? [])
    );

    this.submittedButNotEvaluated$ = combineLatest([this.allItems$, this.evaluatedItems$]).pipe(
      map(([all, evaluated]) => all?.filter(item => !evaluated.some(e => e.user.id === item.user.id)) ?? []),
      map(items => items.filter(item => item?.submission?.id)),
    )

    this.itemsToEvaluate$ = combineLatest([this.submittedButNotEvaluated$, this.usersWithoutSubmission$]).pipe(
      map(([toEval, users]) => [
        ...toEval,
        ...users.map(user => {
          return { activity: this.activity, user } as ActivityItem;
        })
      ])
    )
  }

  @HostListener('window:resize')
  checkWidth(): void {
    const size = window.screen.availWidth;
    console.log({ size })

    if (size > 500) {
      this.screenSize = 'big';
    } else {
      this.screenSize = 'small';
    }
  }

  toggleVisibility() {
    this.collapseList = !this.collapseList;
  }

  reloadPage() {
    location.reload();
  }

  releaseGrades() {
    this.activitiesApi.releaseGrades(this.courseId, this.activity.id).subscribe(_ => {
      this.reloadPage()
    });
  }
}
