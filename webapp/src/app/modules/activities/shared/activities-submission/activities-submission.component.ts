import { Component, Input, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { BehaviorSubject, Observable } from 'rxjs';
import { concatMap, map, take } from 'rxjs/operators';
import { ActivitySubmission } from 'src/app/models/activity-submission.model';
import { Activity, ActivityStates } from 'src/app/models/activity.model';
import { ActivityItemAdvancedSelectors } from 'src/app/state/activity-item/activity-item.advanced.selector';
import { ActivityItemSelectors } from 'src/app/state/activity-item/activity-item.selector';
import { LoginSelectors } from 'src/app/state/login/login.selector';
import { ActivitiesService } from '../../activities.service';

@Component({
  selector: 'app-activities-submission',
  templateUrl: './activities-submission.component.html',
  styleUrls: ['./activities-submission.component.css']
})

export class ActivitiesSubmissionComponent implements OnInit {
  @Input() activity: Activity;
  @Input() activityStatus: ActivityStates;
  translationText = ActivitiesService.translationText.ActivitiesSubmissionComponent;

  submission$: Observable<ActivitySubmission>;

  isEditing = false;
  fetch$ = new BehaviorSubject<boolean>(null);

  public get states(): typeof ActivityStates {
    return ActivityStates;
  }

  constructor(private store: Store) { }

  ngOnInit() {
    this.setObservables();
  }

  setObservables() {
    this.submission$ = this.store.select(LoginSelectors.loggedUserId).pipe(
      take(1),
      concatMap(userId => this.store.select(ActivityItemAdvancedSelectors.sel.one(
        ActivityItemSelectors.byActivityIdAndUserId({
          activityId: this.activity.id,
          userId
        })
      ))),
      map(item => item.submission)
    )
  }

  reload() {
    location.reload();
  }
}
