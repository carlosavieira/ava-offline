import { Component, EventEmitter, Injector, Input, Output } from '@angular/core';
import { Validators } from '@angular/forms';
import { ActivityEvaluation } from 'src/app/models/activity-evaluation.model';
import { ActivityItem } from 'src/app/models/activity-item.model';
import { ActivitiesApiService } from 'src/app/services/api/activities.api.service';
import { Form } from 'src/app/templates/form';
import { ActivitiesService } from '../../activities.service';

@Component({
  selector: 'app-activities-evaluate',
  templateUrl: './activities-evaluate.component.html',
  styleUrls: ['./activities-evaluate.component.css']
})

export class ActivitiesEvaluateComponent extends Form<ActivityEvaluation> {
  translationText = ActivitiesService.translationText.ActivitiesEvaluateComponent;

  // eslint-disable-next-line @angular-eslint/no-input-rename
  @Input() item: ActivityItem;
  @Output() sent: EventEmitter<void> = new EventEmitter<void>();

  default: ActivityEvaluation;

  constructor(
    private injector: Injector,
    private activitiesApi: ActivitiesApiService
  ) {
    super(injector);
  }

  getBlank() {
    return {} as ActivityEvaluation;
  }

  valuePointer() {
    return this.item.evaluation;
  }

  buildForm() {
    return this.fb.group({
      score: [this.value?.score, Validators.required],
      comment: [this.value?.comment],
      submissionID: [this.item.submission?.id]
    });
  }
  onSubmitLocal() {
    if (!this.item.evaluation) {
      this.create();
    } else {
      this.edit()
    }

  }

  create() {
    this.activitiesApi.createEvaluation(
      {
        courseId: this.courseId,
        activityId: this.routeData.activity?.id,
        userId: this.item.user.id,
        form: this.form.value
      }).subscribe(data => {

        if (data.id) {
          console.log('Successfully created');
        }

        this.sent.emit();
      });
  }

  edit() {
    this.activitiesApi.editEvaluation(
      {
        courseId: this.courseId,
        activityId: this.routeData.activity?.id,
        userId: this.item.user.id,
        form: this.form.value
      }).subscribe(data => {

        if (data.id) {
          console.log('Successfully created');
        }

        this.sent.emit();
      });

  }
}
