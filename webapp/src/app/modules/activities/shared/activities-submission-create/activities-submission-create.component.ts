import { Component, EventEmitter, Injector, Input, Output } from '@angular/core';
import { Validators } from '@angular/forms';
import { Store } from '@ngrx/store';
import { ActivitySubmission } from 'src/app/models/activity-submission.model';
import { Activity } from 'src/app/models/activity.model';
import { Course } from 'src/app/models/course.model';
import { SharedService } from 'src/app/modules/shared/shared.service';
import { ActivitiesApiService } from 'src/app/services/api/activities.api.service';
import { ActivitySubmissionActions } from 'src/app/state/activity-submission/activity-submission.actions';
import { FormWithFiles } from 'src/app/templates/form';
import { ActivitiesService } from '../../activities.service';


@Component({
  selector: 'app-activities-submission-create',
  templateUrl: './activities-submission-create.component.html',
  styleUrls: ['./activities-submission-create.component.css']
})
export class ActivitiesSubmissionCreateComponent extends FormWithFiles<ActivitySubmission> {

  translationText = ActivitiesService.translationText.ActivitiesSubmissionCreateComponent;

  activityId: number;

  @Output() sent: EventEmitter<void> = new EventEmitter<void>();
  @Input() submission: ActivitySubmission;


  constructor(private injector: Injector,
    private activitiesApi: ActivitiesApiService,
    private store: Store,
    private sharedService: SharedService
  ) {
    super(injector);
    this.route.data.subscribe((data: { course: Course, activity: Activity }) => {
      this.activityId = data.activity.id;
      this.courseId = data.course.id;

      this.store.dispatch(ActivitySubmissionActions.getMine.request({
        input: {
          activityId: this.activityId,
          courseId: this.courseId
        }
      }))
    });
  }

  buildForm() {
    return this.fb.group({
      answer: [this.value?.answer, Validators.required]
    });
  }

  protected setFilesObservable() {
    this.filesObservable = this.activitiesApi.getSubmissionFiles(this.courseId, this.activityId);
  }

  valuePointer() {
    this.activityId = (this.routeData.activity as Activity)?.id;

    return this.submission;
  }

  protected getBlank() {
    return {} as ActivitySubmission;
  }


  create() {
    const form = this.form.value;

    this.activitiesApi.createSubmission({ form, courseId: this.courseId, activityId: this.activityId }).subscribe((res) => {
      console.log('Submission sent!');

      const urlFiles = `courses/${this.courseId}/activities/${this.activityId}/submission/files/`;


      this.sharedService.uploadFiles(this.filesToUpload, urlFiles).subscribe(_ => {
        console.log('Files sent');

        this.sent.emit();
      });
    });
  }

  edit() {
    const form = this.form.value;

    this.activitiesApi.editSubmission({ form, courseId: this.courseId, activityId: this.activityId }).subscribe((res) => {
      console.log('Submission sent!');

      const url = `courses/${this.courseId}/activities/${this.activityId}/submission/files/`;

      this.sharedService.uploadFileChanges(url, this.filesToDelete, this.filesToUpload).subscribe(_ => {
        console.log('Files sent!');

        this.sent.emit();
      });
    });
  }
}
