import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { CourseSelectors } from 'src/app/state/course/course.selector';
import { ParticipationAdvancedSelectors } from 'src/app/state/participation/participation.advanced.selector';
import { ActivitiesService } from '../../activities.service';

@Component({
  selector: 'app-activities-menu',
  templateUrl: './activities-menu.component.html',
  styleUrls: ['./activities-menu.component.css']
})

export class ActivitiesMenuComponent {

  canCreate$: Observable<boolean>;
  courseId$: Observable<number>;

  translationText = ActivitiesService.translationText.ActivitiesMenuComponent;

  constructor(
    private store: Store
  ) {
    this.courseId$ = this.store.select(CourseSelectors.currentId).pipe(map(id => Number(id)));
    this.canCreate$ = this.store.select(ParticipationAdvancedSelectors.hasPermission('create_activities'))
  }

}
