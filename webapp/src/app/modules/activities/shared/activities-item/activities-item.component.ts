import { animate, state, style, transition, trigger } from '@angular/animations';
import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { IconName, IconPrefix } from '@fortawesome/fontawesome-common-types';
import { Store } from '@ngrx/store';
import deepEqual from 'deep-equal';
import { combineLatest, Observable, of, Subscription } from 'rxjs';
import { concatMap, distinctUntilChanged, map, share, take, tap } from 'rxjs/operators';
import { Activity, ActivityStates } from 'src/app/models/activity.model';
import { ActivityItemSelectors } from 'src/app/state/activity-item/activity-item.selector';
import { ActivitySubmissionActions } from 'src/app/state/activity-submission/activity-submission.actions';
import { ActivityActions } from 'src/app/state/activity/activity.actions';
import { ActivityAdvancedSelectors } from 'src/app/state/activity/activity.advanced.selector';
import { ActivitySelectors } from 'src/app/state/activity/activity.selector';
import { CourseSelectors } from 'src/app/state/course/course.selector';
import { LoginSelectors } from 'src/app/state/login/login.selector';
import { ParticipationAdvancedSelectors } from 'src/app/state/participation/participation.advanced.selector';
import { selectRouteParam } from 'src/app/state/router/router.selector';
import { ActivitiesService } from '../../activities.service';


const blankStatusIcon = ['far', 'circle'] as [IconPrefix, IconName];
const filledStatusIcon = ['fas', 'circle'] as [IconPrefix, IconName]

type IconStatus = {
  text: string;
  textClosed: string;
  icons: [IconPrefix, IconName][];
}

type StatusText = typeof ActivitiesService.translationText.ActivitiesItemComponent.statusText;

@Component({
  selector: 'app-activities-item',
  templateUrl: './activities-item.component.html',
  styleUrls: ['./activities-item.component.css'],
  animations: [
    trigger('openClose', [
      state('open', style({
        width: '10px'
      })),
      state('closed', style({
        width: 'calc(100% - 50px)',
      })),
      transition('open <=> closed', [
        animate('0.15s')
      ]),
    ]),

    trigger('shownHidden', [
      state('shown', style({
        display: 'flex',
        opacity: 1
      })),
      state('hidden', style({
        display: 'none',
        opacity: 0,
      })),
      transition('* => *', [
        animate('0.1s')
      ])
    ]),
  ],
})
export class ActivitiesItemComponent implements OnInit, OnDestroy {

  @Input() item: Activity;

  courseId$: Observable<number>;
  activity: Activity;
  activity$: Observable<Activity>;
  activityStatus$: Observable<ActivityStates>;

  public get states(): typeof ActivityStates {
    return ActivityStates;
  }

  translationText = ActivitiesService.translationText.ActivitiesItemComponent;
  courseId: number;
  files: number;

  isOpen = true;

  status$: Observable<IconStatus>;
  canEdit$: Observable<boolean>;
  canDelete$: Observable<boolean>;

  subscriptions: Subscription[] = [];

  constructor(private store: Store, private activitiesService: ActivitiesService) {

    this.store.select(CourseSelectors.currentId).subscribe(id => {
      this.courseId = id;
    })
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(sub => sub.unsubscribe())
  }

  ngOnInit() {
    this.isOpen = false;
    this.canEdit$ = this.store.select(ParticipationAdvancedSelectors.hasPermission('update_activities'));
    this.canDelete$ = this.store.select(ParticipationAdvancedSelectors.hasPermission('delete_activities'));

    this.activity = this.item;
    this.files = this.item?.files?.length;

    this.setActivityObservables();
  }

  delete(enable: boolean) {
    if (enable) {
      this.store.dispatch(ActivityActions.delete.request({
        input: {
          courseId: this.courseId,
          id: this.activity.id
        }
      }))
    }
  }

  toggle(value?: boolean) {
    if (value === undefined) {
      this.isOpen = !this.isOpen;
    } else {
      this.isOpen = value;
    }
  }

  private setIconsStateObservables() {

    const canEditIsSubmitted$ = this.canEdit$.pipe(
      take(1),
      tap(canEdit => {
        if (!canEdit) {
          this.store.dispatch(ActivitySubmissionActions.getMine.request({
            input: {
              courseId: this.courseId,
              activityId: this.item.id
            }
          }));
        }
      }),
      concatMap(canEdit => canEdit
        ? of({ canEdit, submitted: false })
        : this.store.select(LoginSelectors.loggedUserId).pipe(
          concatMap(userId => this.store.select(ActivityItemSelectors.byActivityIdAndUserId({
            activityId: this.item.id,
            userId
          })).pipe(
            tap(submission => console.log({ submission })),
            map(item => ({ canEdit, submitted: item?.submissionId > 0 }))
          )
          )
        )
      ),
      share(),
    );


    this.status$ = combineLatest([
      of(this.translationText.statusText),
      this.activity$.pipe(
        distinctUntilChanged<Activity>(deepEqual)
      ),
      this.activityStatus$,
      canEditIsSubmitted$
    ]).pipe(
      map(([myText, activity, status, { canEdit, submitted }]) => canEdit
        ? this.getTeacherIcons(activity, status, myText)
        : this.getStudentIcons(submitted, activity, status, myText)
      )
    )
  }

  getCurrentIconPosition(status: IconStatus) {
    let position = 4;

    while (status.icons[position] === blankStatusIcon) {
      position--;
    }

    return position
  }

  private getTeacherIcons(activity: Activity, status: ActivityStates, myText: StatusText) {

    const publicationDateText = ' ' + activity?.publishDate.toLocaleDateString("PT-BR")
    const submissionEndDateText = ' ' + activity?.submissionEnd.toLocaleDateString("PT-BR")

    const icons = {
      Scheduled: {
        text: myText.forTeachers.scheduled,
        textClosed: myText.forTeachers.scheduledClosed + publicationDateText,
        icons: [
          ['far', 'clock'],
          blankStatusIcon,
          blankStatusIcon,
          blankStatusIcon,
          blankStatusIcon,
        ] as [IconPrefix, IconName][],
      },
      Published: {
        text: myText.forTeachers.published,
        textClosed: myText.forTeachers.published,
        icons: [
          filledStatusIcon,
          ['far', 'check-circle'],
          blankStatusIcon,
          blankStatusIcon,
          blankStatusIcon,
        ] as [IconPrefix, IconName][]
      },
      OnSubmissionPeriod: {
        text: myText.forTeachers.submissionPeriod,
        textClosed: myText.forTeachers.submissionPeriodClosed + submissionEndDateText,
        icons: [
          filledStatusIcon,
          filledStatusIcon,
          ['fas', 'user-clock'],
          blankStatusIcon,
          blankStatusIcon,
        ] as [IconPrefix, IconName][]
      },
      Evaluate: {
        text: myText.forTeachers.evaluationPeriod,
        textClosed: myText.forTeachers.evaluationPeriodClosed,
        icons: [
          filledStatusIcon,
          filledStatusIcon,
          filledStatusIcon,
          ['fas', 'edit'],
          blankStatusIcon,
        ] as [IconPrefix, IconName][]
      },
      GradesReleased: {
        text: myText.forTeachers.evaluated,
        textClosed: myText.forTeachers.evaluated,
        icons: [
          filledStatusIcon,
          filledStatusIcon,
          filledStatusIcon,
          filledStatusIcon,
          ['far', 'grades' as IconName],
        ] as [IconPrefix, IconName][]
      },
    }

    switch (status) {
      case (ActivityStates.Scheduled):
        return icons.Scheduled;
      case (ActivityStates.Published):
        return icons.Published;
      case (ActivityStates.SubmissionStarted):
        return icons.OnSubmissionPeriod;
      case (ActivityStates.SubmissionEnded):
        return icons.Evaluate;
      case (ActivityStates.GradesReleased):
        return icons.GradesReleased;
      default:
        if (!activity?.id) {
          return icons.Scheduled;
        }

        throw new Error(`Unknown activity (id=${activity?.id}) state: ${status}`);
    }
  }

  private getStudentIcons(submitted: boolean, activity: Activity, status: ActivityStates, myText: StatusText) {
    const submissionEndDateText = ' ' + activity?.submissionEnd.toLocaleDateString("PT-BR")

    const icons = {
      Published: {
        text: myText.forStudents.published,
        textClosed: myText.forStudents.publishedClosed,
        icons: [
          ['far', 'eye'],
          blankStatusIcon,
          blankStatusIcon,
          blankStatusIcon,
          blankStatusIcon,
        ] as [IconPrefix, IconName][]
      },
      NotSubmittedYet: {
        text: myText.forStudents.submissionPeriod,
        textClosed: myText.forStudents.submissionPeriodClosed + submissionEndDateText,
        icons: [
          filledStatusIcon,
          ['fas', 'user-edit'],
          blankStatusIcon,
          blankStatusIcon,
          blankStatusIcon,
        ] as [IconPrefix, IconName][]
      },
      OnPeriodSubmitted: {
        text: myText.forStudents.submitted,
        textClosed: myText.forStudents.submissionPeriodClosed + submissionEndDateText,
        icons: [
          filledStatusIcon,
          filledStatusIcon,
          ['fas', 'user-check'],
          blankStatusIcon,
          blankStatusIcon,
        ] as [IconPrefix, IconName][]
      },
      WaitingForEvaluation: {
        text: myText.forStudents.submitted,
        textClosed: myText.forStudents.evaluationPeriodClosed,
        icons: [
          filledStatusIcon,
          filledStatusIcon,
          filledStatusIcon,
          ['fas', 'user-clock'],
          blankStatusIcon,
        ] as [IconPrefix, IconName][]
      },
      WaitingForEvaluationWithoutSubmission: {
        text: myText.forStudents.evaluationNotSubmitted,
        textClosed: myText.forStudents.evaluationPeriodClosed,
        icons: [
          filledStatusIcon,
          filledStatusIcon,
          ['fas', 'times'],
          ['fas', 'user-clock'],
          blankStatusIcon,
        ] as [IconPrefix, IconName][]
      },
      GradesReleased: {
        text: myText.forStudents.evaluated,
        textClosed: myText.forStudents.evaluated,
        icons: [
          filledStatusIcon,
          filledStatusIcon,
          filledStatusIcon,
          filledStatusIcon,
          ['far', 'grades' as IconName],
        ] as [IconPrefix, IconName][]
      },
      GradesReleasedWithoutSubmission: {
        text: myText.forStudents.evaluated,
        textClosed: myText.forStudents.evaluated,
        icons: [
          filledStatusIcon,
          filledStatusIcon,
          ['fas', 'times'],
          filledStatusIcon,
          ['far', 'grades' as IconName],
        ] as [IconPrefix, IconName][]
      }
    }

    switch (status) {
      case (ActivityStates.Published):
        return icons.Published;
      case (ActivityStates.SubmissionStarted):
        return submitted
          ? icons.OnPeriodSubmitted
          : icons.NotSubmittedYet;
      case (ActivityStates.SubmissionEnded):
        return submitted
          ? icons.WaitingForEvaluation
          : icons.WaitingForEvaluationWithoutSubmission;
      case (ActivityStates.GradesReleased):
        return submitted
          ? icons.GradesReleased
          : icons.GradesReleasedWithoutSubmission;
      default:
        throw new Error(`Unknown state for activity id=${activity.id} state: ${status}`);
    }
  }

  private setActivityObservables() {

    this.courseId$ = this.store.select(selectRouteParam('courseId')).pipe(
      map(id => Number(id))
    )

    this.subscriptions.push(
      this.courseId$.subscribe(id => this.courseId = id)
    )

    this.store.dispatch(ActivityActions.listFiles.request({
      input: {
        courseId: this.courseId,
        id: this.activity.id
      }
    }))

    this.activity$ = this.store.select(ActivityAdvancedSelectors.sel.one(ActivitySelectors.byId(this.activity.id)))

    this.activityStatus$ = this.activity$.pipe(
      map(activity => this.activitiesService.getActivityState(activity))
    );

    this.setIconsStateObservables()
  }
}
