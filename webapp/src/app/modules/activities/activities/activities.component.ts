import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Activity } from 'src/app/models/activity.model';
import { ActivityActions } from 'src/app/state/activity/activity.actions';
import { ActivityAdvancedSelectors } from 'src/app/state/activity/activity.advanced.selector';
import { ActivitySelectors } from 'src/app/state/activity/activity.selector';
import { ActivitiesService } from './../activities.service';


@Component({
  selector: 'app-activity',
  styleUrls: ['activities.component.css'],
  templateUrl: 'activities.component.html'
})
export class ActivitiesComponent implements OnInit {
  translationText = ActivitiesService.translationText.ActivitiesComponent;
  activities$: Observable<Activity[]>

  constructor(private store: Store) {

    this.activities$ = this.store.select(ActivityAdvancedSelectors.sel.many(ActivitySelectors.byCourse.current.all)).pipe(
      map(all => all.sort(this.sortActivitiesByPublishDate)
      )
    );
  }

  ngOnInit(): void {
    this.fetchAll();
  }

  fetchAll(): void {
    this.store.dispatch(ActivityActions.fromCurrentCourse.fetchAll.request())
  }

  trackActivity(index: number, activity: Activity) {
    return activity?.id;
  }

  private sortActivitiesByPublishDate(a: Activity, b: Activity) {
    return a.submissionEnd < b.submissionEnd ? 1 : (a.submissionEnd > b.submissionEnd ? -1 : 0);
  }
}
