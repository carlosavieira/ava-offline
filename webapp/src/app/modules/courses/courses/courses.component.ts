import { Component, HostListener, OnDestroy } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable, Subscription } from 'rxjs';
import { LoginSelectors } from 'src/app/state/login/login.selector';
import { AppState } from 'src/app/state/state';

@Component({
  selector: 'app-course',
  templateUrl: './courses.component.html',
  styleUrls: ['./courses.component.css']
})
export class CoursesComponent implements OnDestroy {
  innerHeight = 0;

  subs$: Subscription[] = [];

  isOffline$: Observable<boolean>

  constructor(private store: Store<AppState>) {
    this.isOffline$ = this.store.select(LoginSelectors.isOffline);

    this.getScreenSize();
  }

  ngOnDestroy(): void {
    this.subs$.forEach(s => s.unsubscribe());
  }

  @HostListener('window:resize', ['$event'])
  getScreenSize(event?) {
    this.innerHeight = window.innerHeight;
  }
}
