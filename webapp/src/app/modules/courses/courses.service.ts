import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { LANGUAGE } from 'src/app/dev/languages';
import { CourseSM } from 'src/app/models/course.model';
import { Role } from 'src/app/models/role.model';
import { User } from 'src/app/models/user.model';
import { CourseKeyApiService } from 'src/app/services/api/course-key.api.service';
import { CourseSelectors } from 'src/app/state/course/course.selector';
import { RoleSelectors } from 'src/app/state/role/role.selector';
import { UserSelectors } from 'src/app/state/user/user.selector';

@Injectable({
  providedIn: 'root'
})
export class CoursesService {

  static translationText = LANGUAGE.home;

  course$: Observable<any>;
  user$: Observable<User>;
  role$: Observable<Role>;

  constructor(private store: Store,
    private courseKeyApiService: CourseKeyApiService,
  ) {
    this.setObservables();
  }

  enrollCourseByKey(key: string): Observable<any> {
    return this.courseKeyApiService.enrollCourseByKey(key);
  }

  findCourseByKey(key: string): Observable<CourseSM> {
    return this.courseKeyApiService.findCourseByKey(key);
  }

  hasPermission(permission: string) {
    return this.role$.pipe(
      map(role => role?.permissions?.findIndex(p => p?.name === permission) >= 0)
    )
  }

  private setObservables() {
    this.course$ = this.store.select(CourseSelectors.current);
    this.user$ = this.store.select(UserSelectors.current);
    this.role$ = this.store.select(RoleSelectors.current);
  }
}
