import { Component, EventEmitter, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { of } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { Course, fromJsonToCourse } from 'src/app/models/course.model';
import { CoursesService } from '../courses.service';

@Component({
  selector: 'app-home-enter-course',
  templateUrl: './courses-home-enter.component.html',
  styleUrls: ['./courses-home-enter.component.css']
})
export class CoursesHomeEnterComponent {

  form: FormGroup;
  course: Course;
  courseError: boolean;
  isProcessing: boolean;

  @Output() closeForm = new EventEmitter<void>();

  constructor(private coursesService: CoursesService, private fb: FormBuilder, private router: Router, private store: Store) {
    this.form = this.fb.group({
      key: ['', [Validators.maxLength(6), Validators.required, Validators.minLength(6)]],
    });

    this.form.valueChanges.subscribe(value => {

      if (value.key.length === 6) {
        this.isProcessing = true;
        this.courseError = false;
        this.coursesService.findCourseByKey(value.key).pipe(
          map(data => fromJsonToCourse(data)),
          catchError((error) => {
            return of(null);
          })
        ).subscribe(course => {
          if (course) {
            this.course = course;
            this.courseError = false;
          } else {
            this.courseError = true;
          }
          this.isProcessing = false;
        });
      } else {
        this.isProcessing = false;
        this.courseError = false;
        this.course = null;
      }
    });

  }



  onSubmit(form) {
    this.coursesService.enrollCourseByKey(form.key).subscribe((data) => {
      this.closeForm.emit();
      this.router.navigate([`/courses/${this.course?.id}`], {
        queryParams: { refresh: true }
      });
    });
  }

  reset() {
    this.form.reset();
    this.course = null;
  }
}
