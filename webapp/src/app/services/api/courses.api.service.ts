import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { take } from 'rxjs/operators';
import { CourseSM } from 'src/app/models/course.model';

@Injectable({
  providedIn: 'root'
})
export class CoursesApiService {

  constructor(private http: HttpClient) { }


  getCourses(): Observable<CourseSM[]> {
    return this.http.get<CourseSM[]>('courses').pipe(take(1));
  }

  getCourse(courseId: number | string): Observable<CourseSM> {
    const url = `courses/${courseId}`;

    return this.http.get<CourseSM>(url).pipe(take(1));
  }

  deleteCourse(courseId: number): Observable<any> {
    const url = `courses/${courseId}`;

    return this.http.delete(url).pipe(take(1));
  }

  updateCourse(courseId: number, body: CourseSM): Observable<CourseSM> {
    const url = `courses/${courseId}`;

    return this.http.put<CourseSM>(url, body).pipe(take(1));
  }

  createCourse(body: CourseSM): Observable<CourseSM> {
    const url = `courses/`;

    return this.http.post<CourseSM>(url, body).pipe(take(1));
  }

  // List by Role

  getCoursesAsTeacher(userId: number): Observable<CourseSM[]> {
    const url = `users/${userId}/roles/3`;

    return this.http.get<CourseSM[]>(url).pipe(take(1));
  }

  getCoursesAsStudent(userId: number): Observable<CourseSM[]> {
    const url = `users/${userId}/roles/2`;

    return this.http.get<CourseSM[]>(url).pipe(take(1));
  }

}
