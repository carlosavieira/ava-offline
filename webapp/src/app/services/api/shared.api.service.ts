import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of, zip } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { UploadedFile } from 'src/app/models/file';

@Injectable({
  providedIn: 'root'
})
export class SharedApiService {

  constructor(private http: HttpClient) { }


  public getUrlTitle(link: string): Observable<string> {
    const url = 'https://textance.herokuapp.com/rest/title/' + link;

    return this.http.get(url, { responseType: 'text' }).pipe(
      map((data: string) => {
        console.log('O título da página é: ' + data);

        return data;
      }),
      catchError((err: HttpErrorResponse) => {
        console.log('Não foi possível encontrar o título da página: ' + link);
        return of(null);
      }),
    );
  }


  /* ----------------------------- File methods ----------------------------- */

  public uploadFiles(files: File[], url: string): Observable<any> {
    // If there is no file to upload, just return
    if (!files || files.length === 0) {
      return of(null);
    }

    const formData: FormData = new FormData();


    files.forEach(item => {
      formData.append('files', item, item?.name);
    });

    return this.http.post(url, formData);
  }


  /**
   * Send a delete request to the API
   *
   * @param delString - path to delete something
   */
  deleteFile(delString: string) {
    return this.http.delete(delString);
  }

  /**
   * Delete a list of files inside the same "directory"
   *
   * @param baseString - path ending with /
   */
  deleteFiles(baseString: string, names: string[]): Observable<void> {

    return zip(...names.map(name => this.http.delete(baseString + name))).pipe(
      map(_ => { })
    );
  }

  uploadFileChanges(url: string, toDelete: UploadedFile[], upload: File[]): Observable<any> {
    const observables: Observable<any>[] = [];

    if (toDelete && toDelete.length > 0) {
      const names = toDelete.map(file => encodeURIComponent(file.fileName));
      observables.push(this.deleteFiles(url, names));
    }

    if (upload && upload.length > 0) {
      observables.push(this.uploadFiles(upload, url));
    }

    if (observables.length === 0) {
      return of(null);
    }

    return zip(...observables);
  }
}
