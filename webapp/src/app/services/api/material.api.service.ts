import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { catchError, map, take } from 'rxjs/operators';
import { MaterialForm, MaterialJson } from 'src/app/models/material.model';
import { patchFilesWithUrl } from 'src/app/state/file-uploaded/file-uploaded.state';

@Injectable({
  providedIn: 'root'
})
export class MaterialApiService {

  constructor(private http: HttpClient) { }

  getMaterials(courseId: number): Observable<MaterialJson[]> {
    const url = `courses/${courseId}/materials`

    return this.http.get<MaterialJson[]>(url).pipe(
      take(1),
      map(items => items.map(item => ({
        ...item,
        files: patchFilesWithUrl(item?.files, `${url}/${item.id}/files`)
      })))
    );
  }

  editMaterialLink({ courseId, materialId, body }: { courseId: number; materialId: number; body: MaterialForm; }): Observable<MaterialJson> {
    const url = `courses/${courseId}/materials/${materialId}`;

    return this.http.put<MaterialJson>(url, body).pipe(take(1));
  }

  deleteMaterial({ courseId, materialId }: { courseId: number; materialId: number; }): Observable<void> {
    const url = `courses/${courseId}/materials/${materialId}`;

    return this.http.delete<void>(url).pipe(take(1));
  }

  create({ body, courseId }: { body: MaterialForm; courseId: number; }): Observable<MaterialJson> {
    const url = `courses/${courseId}/materials`;

    return this.http.post<MaterialJson>(url, body).pipe(take(1), map(item => ({
      ...item,
      files: patchFilesWithUrl(item?.files, `${url}/${item.id}/files`)
    })));
  }

  createFiles({ files, courseId }: { files: File[]; courseId: number; }): Observable<MaterialJson[]> {
    const url = `courses/${courseId}/materials/files`;

    return this.uploadMaterials({ files, url }).pipe(take(1), map(items => items.map(item => ({
      ...item,
      files: patchFilesWithUrl(item?.files, `courses/${courseId}/materials/${item.id}/files`)
    }))));
  }

  private uploadMaterials({ files, url }: { files: File[]; url: string; }): Observable<MaterialJson[]> {
    // If there is no file to upload, just return
    if (!files || files.length === 0) {
      return of(null);
    }

    const formData: FormData = new FormData();
    files.forEach(item => {
      formData.append('files', item, item?.name);
    });

    return this.http.post<MaterialJson[]>(url, formData);
  }


  public getUrlTitle(link: string): Observable<string> {
    const url = 'https://textance.herokuapp.com/rest/title/' + link;

    return this.http.get(url, { responseType: 'text' }).pipe(
      map((data: string) => {
        console.log('O título da página é: ' + data);

        return data;
      }),
      catchError((err: HttpErrorResponse) => {
        console.log('Não foi possível encontrar o título da página: ' + link);
        return of(null);
      }),
    );
  }
}
