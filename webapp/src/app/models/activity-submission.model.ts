import { getFileId } from '../state/file-uploaded/file-uploaded.state';
import { FileUploaded, FileUploadedJson, HaveFilesSM } from './file-uploaded.model';
import { Created, CreatedJson, CreatedSM, fromJsonToCreatedSM, fromJsonToLastModifiedSM, LastModified, LastModifiedJson, LastModifiedSM } from './index';

export interface ActivitySubmissionForm {
  answer?: string;
}

export interface ActivitySubmissionJson extends LastModifiedJson, CreatedJson {
  id?: number;
  answer?: string;
  files?: FileUploadedJson[];
}

export interface ActivitySubmission extends LastModified, Created {
  id?: number;
  answer?: string;
  files?: FileUploaded[];
}


export const fromJsonToActivitySubmissionSM: (json: ActivitySubmissionJson) => ActivitySubmissionSM = (json: ActivitySubmissionJson) => {
  if (!json) {
    return undefined;
  }

  const { files, ...rest } = json;

  return !rest ? undefined : {
    ...rest,
    files: json?.files?.map(file => getFileId(file)),
    ...fromJsonToLastModifiedSM(rest),
    ...fromJsonToCreatedSM(rest),
  };
}

export interface ActivitySubmissionSM extends LastModifiedSM, CreatedSM, HaveFilesSM {
  id?: number;
  answer?: string;
}
