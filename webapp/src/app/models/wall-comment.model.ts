import { CreatedJson, CreatedSM, fromJsonToCreatedSM, fromJsonToLastModifiedSM, LastModifiedSM } from ".";
import { Convert, Created, fromJsonToCreated, fromJsonToLastModified, LastModified, LastModifiedJson } from './index';

export interface WallCommentForm {
  text: string;
}

export interface WallCommentJson extends CreatedJson, LastModifiedJson {
  id: number;
  text: string;

  liked: boolean;
  likeCounter: number;
  teacher: boolean;
}

export interface WallCommentSM extends CreatedSM, LastModifiedSM {
  id: number;
  text: string;

  liked: boolean;
  likeCounter: number;
  teacher: boolean;
}

export interface WallComment extends Created, LastModified {
  id: number;
  text: string;

  liked: boolean;
  likeCounter: number;
  teacher: boolean;
}

export const fromJsonToWallComment: Convert<WallCommentJson, WallComment> = (json: WallCommentJson) => {
  return (!json) ? undefined : {
    ...json,
    ...fromJsonToCreated(json),
    ...fromJsonToLastModified(json),
  };
}

export const fromJsonToWallCommentSM: Convert<WallCommentJson, WallCommentSM> = (data) => {
  const { createdBy, lastModifiedBy, ...rest } = data;

  return (!rest) ? undefined : {
    ...rest,
    ...fromJsonToCreatedSM(data),
    ...fromJsonToLastModifiedSM(data),
  };
}
