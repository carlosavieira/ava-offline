import { NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { Created, CreatedJson, CreatedSM, fromJsonToCreatedSM, fromJsonToLastModifiedSM, LastModified, LastModifiedJson, LastModifiedSM } from '.';
import { getFileId } from '../state/file-uploaded/file-uploaded.state';
import { FileUploaded, FileUploadedJson, HaveFilesSM } from './file-uploaded.model';


// Order matters so we can use:
// status >= ActivityStates.Published
export enum ActivityStates {
  Scheduled,
  Published,
  SubmissionStarted,
  SubmissionEnded,
  GradesReleased
}

export interface ActivityJson extends CreatedJson, LastModifiedJson {
  id: number;
  title: string;
  description: string;
  criteria: string;
  files?: FileUploadedJson[];
  gradeWeight: number;
  gradesReleaseDate: string;
  publishDate: string;
  submissionBegin: string;
  submissionEnd: string;
}

export interface Activity extends Created, LastModified {
  id: number;
  title: string;
  description: string;
  criteria: string;
  files?: FileUploaded[];
  gradeWeight: number;
  gradesReleaseDate?: Date;
  publishDate: Date;
  submissionBegin: Date;
  submissionEnd: Date;
}

export const fromJsonToActivitySM: (json: ActivityJson) => ActivitySM = (json: ActivityJson) => {
  const { files, ...rest } = json;

  return (!json) ? undefined : {
    ...rest,
    ...fromJsonToCreatedSM(json),
    ...fromJsonToLastModifiedSM(json),
    files: files?.map(file => getFileId(file))
  };
}

export interface ActivityForm {
  title: string;
  gradeWeight?: number;
  description: string;
  submissionPeriod: {
    end: NgbDateStruct,
    start: NgbDateStruct,
  };
  publishDate: NgbDateStruct;
  hasGrade: boolean;
  criteria: string;
  files: EditFilesForm,
}

export interface ActivityFormJson {
  title: string;
  gradeWeight?: number;
  description: string;
  submissionBegin: Date;
  submissionEnd: Date;
  publishDate: Date;
  hasGrade: boolean;
  criteria: string;
}

export const convertFromNgb = (date: NgbDateStruct, isEndOfDay: boolean): Date => {
  if (isEndOfDay) {
    return new Date(date.year, date.month - 1, date.day, 23, 59, 59);
  }

  return new Date(date.year, date.month - 1, date.day);
}

export const fromActivityFormToJson: (form: ActivityForm) => ActivityFormJson = (form: ActivityForm) => {
  const { submissionPeriod, publishDate, files, ...rest } = form;

  return {
    ...rest,
    submissionBegin: convertFromNgb(submissionPeriod.start, false),
    submissionEnd: convertFromNgb(submissionPeriod.end, true),
    publishDate: convertFromNgb(publishDate, false),
  }
}

export const patchActivityFiles = (activity: ActivityJson, courseId: number) => ({
  ...activity,
  files: activity?.files?.map(file =>
  ({
    ...file,
    downloadUri: `courses/${courseId}/activities/${activity.id}/files/${encodeURIComponent(file.fileName)}`
  })) ?? []
})

export type EditFilesForm = {
  uploaded: FileUploaded[];
  toDelete: FileUploaded[];
  toUpload: File[];
}

export enum ActivitySortBy {
  MostRecentPublication,
  LastModified,
  OlderPublication
}

export enum ActivityFilter {
  NoFilter,
  Evaluated,
  ToEvaluate,
  ToDo,
  Done,
  Ended
}

export interface ActivitySM extends CreatedSM, LastModifiedSM, HaveFilesSM {
  id: number;
  title: string;
  description: string;

  publishDate: string;
  submissionBegin: string;
  submissionEnd: string;

  criteria: string;
  gradeWeight: number;
  gradesReleaseDate: string;
}
