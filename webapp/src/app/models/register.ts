export interface RegisterStart {
    email: string;
    name: string;
    password: string;
}

export interface RegisterFinish {
    email: string;
    hash: string;
}