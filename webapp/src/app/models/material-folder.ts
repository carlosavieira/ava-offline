import { Convert } from './index';


export interface MaterialFolderForm {
  title?: string;
  description?: string;
}

export interface MaterialFolderJson {
  id?: number;
  title?: string;
  description?: string;
}

export interface MaterialFolder {
  id?: number;
  title?: string;
  description?: string;
}

export const fromJsonToMaterialFolder: Convert<MaterialFolderJson, MaterialFolder> = (json: MaterialFolderJson) => {
  return (!json) ? undefined : {
    ...json
  };
}
