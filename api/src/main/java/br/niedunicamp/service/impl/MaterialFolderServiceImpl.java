package br.niedunicamp.service.impl;

//#region Imports
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import br.niedunicamp.model.Course;
import br.niedunicamp.model.Material;
import br.niedunicamp.model.MaterialFolder;
import br.niedunicamp.pojo.MaterialFolderDTO;
import br.niedunicamp.repository.CourseRepository;
import br.niedunicamp.repository.MaterialFolderRepository;
import br.niedunicamp.repository.MaterialRepository;
import br.niedunicamp.repository.UserRepository;
import br.niedunicamp.service.CoreService;
import br.niedunicamp.service.FileStorageService;
import br.niedunicamp.service.MaterialFolderService;
//#endregion

@Service
public class MaterialFolderServiceImpl implements MaterialFolderService {

    // #region Repos and Services
    @Autowired
    MaterialFolderRepository materialFolderRepository;

    @Autowired
    MaterialRepository materialRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    CourseRepository courseRepository;

    @Autowired
    FileStorageService fileStorageService;

    @Autowired
    CoreService coreService;
    // #endregion

    @Override
    public ResponseEntity<MaterialFolder> create(Course course, MaterialFolderDTO folderDTO, UserDetails userDetails) {
        MaterialFolder folder = new MaterialFolder();
        folder.setCourse(course);
        folder.setTitle(folderDTO.getTitle());
        folder.setDescription(folderDTO.getDescription());

        return ResponseEntity.ok(materialFolderRepository.save(folder));
    }

    @Override
    public ResponseEntity<List<MaterialFolder>> list(Course course) {

        List<MaterialFolder> list = materialFolderRepository.findByCourse(course);

        return ResponseEntity.ok(list);
    }

    @Override
    public ResponseEntity<MaterialFolder> update(MaterialFolderDTO materialDTO, MaterialFolder folder,
            UserDetails userDetails) {

        folder.setDescription(materialDTO.getDescription());
        folder.setTitle(materialDTO.getTitle());

        return ResponseEntity.ok(materialFolderRepository.save(folder));
    }

    @Override
    public ResponseEntity<?> delete(MaterialFolder folder) {

        materialFolderRepository.delete(folder);

        return ResponseEntity.ok(null);
    }

    @Override
    public ResponseEntity<List<Material>> list(MaterialFolder folder) {

    List<Material> list = materialRepository.findByFolder(folder);

    return ResponseEntity.ok(list.stream().map(item -> {
    item.setFiles(fileStorageService.listFiles(item.getFilesFolder()).getBody());
    return item;
    }).collect(Collectors.toList()));
    }

}