package br.niedunicamp.service;

import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import br.niedunicamp.model.Course;
import br.niedunicamp.model.Material;
import br.niedunicamp.model.MaterialFolder;
import br.niedunicamp.pojo.MaterialFolderDTO;

/**
 * MaterialService
 */
@Service
public interface MaterialFolderService {
    ResponseEntity<MaterialFolder> create(Course course, MaterialFolderDTO folderDTO, UserDetails userDetails);

    ResponseEntity<List<MaterialFolder>> list(Course course);

    ResponseEntity<List<Material>> list(MaterialFolder folder);

    ResponseEntity<MaterialFolder> update(MaterialFolderDTO folderDTO, MaterialFolder folder, UserDetails userDetails);

    ResponseEntity<?> delete(MaterialFolder folder);

}