package br.niedunicamp.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class RoleAndCourseIds {
    private Long courseId;
    private Long roleId;
}
