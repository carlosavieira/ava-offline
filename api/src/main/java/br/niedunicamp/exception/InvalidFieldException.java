package br.niedunicamp.exception;

public class InvalidFieldException extends RuntimeException {

    private static final long serialVersionUID = 4563660440838847292L;

    public InvalidFieldException() {
        super();
    }
    public InvalidFieldException(String message){
        super(message);
    }
}
